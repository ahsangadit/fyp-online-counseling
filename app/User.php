<?php

namespace App;

use Illuminate\Support\Facades\Cache;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','DOB','City','Country','Qualification','Nickname','Gender','Profile_img','Designation','Certification','Skills','Cell_no','User_type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function customer(){
        return $this->hasOne('App\customer');
    }

    public function counselor(){
        return $this->hasOne('App\counselor');
    }

    public function administrator(){
        return $this->hasOne('App\administrator');
    }


    public function isOnline(){
        return Cache::has('user-is-online-'.$this->id);
    }


}
