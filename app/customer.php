<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class customer extends Model
{
    //
    protected $fillable = ['user_id','talks_counts'];


    public function subscribers(){
        return $this->belongsToMany('App\subscriber');
    }

}
