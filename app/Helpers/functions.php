<?php
/**
 * Created by PhpStorm.
 * User: Yasin-PC
* Date: 2/16/2019
* Time: 11:00 PM
*/


use Illuminate\Support\Facades\Auth;

function web_title(){
    return "Counselling Portal";
}


function custom_printR($value){
    echo "<pre>";
    print_r($value);
    echo "</pre>";
}


function profile_img_placeholder(){
    return asset('images/frontend_images/profile/default.png');
}

function profile_img(){
    $user = Auth::user();

    if($user->user_type == "customer"){

        $user_directory = replaceByDash($user->name);
        return asset('images/frontend_images/profile/customers/'.$user_directory.'_'.$user->id.'/'.Auth::user()->profile_img);

    }elseif($user->user_type == "counselor"){

        $user_directory = replaceByDash($user->name);
        return asset('images/frontend_images/profile/counselors/'.$user_directory.'_'.$user->id.'/'.Auth::user()->profile_img);

    }elseif($user->user_type == 'administrator'){

        $user_directory = replaceByDash($user->name);
        return asset('images/frontend_images/profile/administrators/'.$user_directory.'_'.$user->id.'/'.Auth::user()->profile_imgf);

    }else{

    }

}

function setProfilePicture(){
    if(Auth::user()->profile_img == 'default.png'){
        return profile_img_placeholder();
    }else{
        return profile_img();
    }
}


function replaceByDash($value){
    return str_replace(" ","_",$value);
}