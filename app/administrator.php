<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class administrator extends Model
{
    //

    protected $fillable = ['user_id','access_right'];
}
