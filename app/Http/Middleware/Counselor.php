<?php
/**
 * Created by PhpStorm.
 * User: Yasin-PC
 * Date: 3/10/2019
 * Time: 3:56 PM
 */

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Counselor
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request , Closure $next){

        if(Auth::check() && Auth::user()->user_type == "counselor"){
            return $next($request);
        }else{
            return redirect()->action("_LoginController@login")->with('flash_message_error','Please login to access');
        }

    }


}