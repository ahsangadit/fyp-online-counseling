<?php

namespace App\Http\Controllers;

use App\counselor;
use App\Customer;
use App\subscriber;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Jleon\LaravelPnotify\Notify;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function customerLayoutDesign(){
        return view('layouts.customer_layout.customer_design',array('user_detail'=>Auth::user()));
    }

    public function portal(){

//        $customer = customer::where(['user_id'=>Auth::user()->id])->first();
//        $subscribing = subscriber::where(['customer_id'=>$customer->id])->where(['status'=>'Followed'])->count();

//        $subscribing = isset($subscribing) ? $subscribing : "";


        return view('customer.dashboard');
    }





    public function profile(){
        return view('customer.profile');
    }

    public function updateProfile(Request $request){

        $user = Auth::user();

        if($file = $request->file('profile_img')){

            $filename = $file->getClientOriginalName();
            $user_directory = replaceByDash($user->name);
            $file->move('images/frontend_images/profile/customers/'.$user_directory.'_'.$user->id.'/',$filename);
            $input = $filename;
            User::where('id',Auth::user()->id)->update(['profile_img'=>$input]);
        }

        Notify::success('Picture Updated Successfully', 'Profile Picture');
        return redirect('/Customer/Profile');
    }

    public function editProfile(){
        return view('customer.setting-editProfile',array('user_detail'=>Auth::user()));
    }

    public function updateProfileDetails(Request $request){

        $user_id = Auth::user()->id;

        $this->validate($request,[
            'name'=> 'required|min:5|max:15','nickname'=>'min:5',
            'city' => 'required','country'=>'required','skills'=>'required',
            'cell'=> 'required','designation'=>'required','qualification'=>'required','certification'=>'required',
        ]);

        User::where(['id'=>$user_id])->update(["name"=>$request->name , "nickname"=>$request->nickname , 'city'=>$request->city ,
            "country"=>$request->country , "skills"=>$request->skills ,"cell_no" => $request->cell ,
            "designation" => $request->designation , "qualification" => $request->qualification,
            "certification" => $request->certification
        ]);

        Notify::success('Record Updated Successfully', 'Profile Updated');
        return redirect()->action('CustomerController@editProfile');

    }

    public function changePassword(){
        return view('customer.setting-changePassword');
    }

    public function searchCounselors(){
        return view('customer.searchCounselors');
    }

    public function searchingCounselors(Request $request){

            //return  $request->industry ." ".$request->functional_area." ".$request->experience;

            //select users.*, users.id as User_ID , counselors.* , counselors.id as Counselor_ID from users join counselors on users.id = counselors.user_id where
            //counselors.industry = ''
            //or counselors.functional_area = 'Computer Networking'
            //or counselors.experience = ''

            $industry =  isset($request->industry) ? $request->industry : '';
            $functional_area =   isset($request->functional_area) ? $request->functional_area : '';
            $experience = isset($request->experience) ? $request->experience : '';


            $result = DB::table('users')
                        ->join('counselors','users.id','=','counselors.user_id')
                        ->select('users.id','users.*','counselors.id','counselors.*', DB::raw('users.id as User_ID') ,DB::raw('counselors.id as Counselor_ID'))
                        ->where(['counselors.industry'=> $industry ])
                        ->orWhere(['counselors.functional_area'=> $functional_area])
                        ->orWhere(['counselors.experience'=> $experience ])
                        ->get();

            //  echo "<pre>";
            //  print_r($result);
            //  echo "</pre>";
            //
            //  foreach($result as $key => $value){
            //   echo $value->name;
            //  }

            //Notify::success('Filtered Successfully', 'Filtered');
            Session::flash('results',$result);
            return redirect()->action('CustomerController@searchCounselors');

    }

    public function subscribe(Request $request){
        $customer_id =  isset($request->customer_id) ? $request->customer_id : '';
        $counselor_id = isset($request->counselor_id) ? $request->counselor_id: '';

        $subscribe = subscriber::where(['customer_id'=>$customer_id])->where(['counselor_id'=>$counselor_id])->first();

        if(!empty($customer_id) && !empty($counselor_id)){
            if(empty($subscribe->customer_id) && empty($subscribe->counselor_id)){
                $sub = subscriber::create(['customer_id'=>$customer_id,'counselor_id'=>$counselor_id,'status'=>'Followed','attribute'=>'checked']);
                DB::table('customer_subscriber')->insert(['customer_id'=>$customer_id,'subscriber_id'=>$sub->id]);
                // echo 'true'; die;
            }else{
                subscriber::where(['counselor_id'=>$counselor_id])->update(['customer_id'=>$customer_id,'counselor_id'=>$counselor_id,'status'=>'Followed','attribute'=>'checked']);
                //echo 'true'; die;
            }
        }
    }

    public function unsubscribe(Request $request){
        $customer_id =  isset($request->customer_id) ? $request->customer_id : '';
        $counselor_id = isset($request->counselor_id) ? $request->counselor_id: '';

        $subscribing = new subscriber();
        if(!empty($customer_id) && !empty($counselor_id)){
            subscriber::where(['counselor_id'=>$counselor_id])->update(['customer_id'=>$customer_id,'counselor_id'=>$counselor_id,'status'=>'Unfollowed','attribute'=>'unchecked']);
          //  echo 'true'; die;
        }
    }


    public function subscribedCounselors(){


        $customer = customer::where(['user_id'=>Auth::user()->id])->first();
       // $subscribed_counselor_id = subscriber::where(['customer_id'=>$customer->id])->where(['status'=>'Followed'])->get();
       // $Subscribed_counselors = counselor::where(['id' => $subscribed_counselor_id->counselor_id])->first();

        $result = DB::table('users')
            ->join('counselors','users.id','=','counselors.user_id')
            ->join('subscribers','counselor_id','=','counselors.id')
            ->select('users.*','counselors.*','subscribers.*', DB::raw('users.id as User_ID','counselors.id as Counselor_ID'))
            ->where(['subscribers.customer_id'=> $customer->id ])->where(['status'=>'Followed'])
            ->get();


        return view('customer.subscribedCounselors',array('subscribers'=>$result));

    }


    public function singleCounselorProfile(Request $request){

        $user = \App\User::where(['id'=>$request->user_id])->first();
        $user_further_detail = \App\counselor::where(['user_id'=>$user->id])->first();
        $counselor_user_id = $request->user_id;

        return view("customer.singleCounselorProfile",array('counselor_user_id'=>$counselor_user_id,'counselor'=>$user , 'counselor_further_detail'=>$user_further_detail));
    }



    public function joinEvents(){
        return view('customer.join-events');
    }

    public function following(){
        return view('customer.following');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show(Customer $customer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function edit(Customer $customer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Customer $customer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Customer $customer)
    {
        //
    }
}
