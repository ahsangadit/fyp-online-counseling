<?php

namespace App\Http\Controllers;

use App\Administrator;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Jleon\LaravelPnotify\Notify;

class AdministratorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }


    public function adminLayoutDesign(){
        return view('layouts.admin_layout.admin_design',array('user_detail'=>Auth::user()));
    }

    public function portal(){
        $userStatus = User::all();
        return view('admin.dashboard',compact('userStatus'));
//        return view('home',compact('userStatus'));
    }



    public function profile(){
        return view('admin.profile');
    }

    public function editProfile(){
        return view('admin.setting-editProfile',array('user_detail'=>Auth::user()));
    }

    public function updateProfilePicture(Request $request){

        $user = Auth::user();

        if($file = $request->file('profile_img')){

            $filename = $file->getClientOriginalName();
            $user_directory = replaceByDash($user->name);
            $file->move('images/frontend_images/profile/administrator/'.$user_directory.'_'.$user->id.'/',$filename);
            $input = $filename;
            User::where('id',Auth::user()->id)->update(['profile_img'=>$input]);
        }
        return redirect('/Admin/Profile');
    }


    public function updateProfileDetails(Request $request){

        $user_id = Auth::user()->id;

        $this->validate($request,[
            'name'=> 'required|min:5|max:15','nickname'=>'min:5',
            'city' => 'required','country'=>'required','skills'=>'required',
            'cell'=> 'required','designation'=>'required','qualification'=>'required','certification'=>'required',

        ]);


        User::where(['id'=>$user_id])->update(["name"=>$request->name , "nickname"=>$request->nickname , 'city'=>$request->city ,
            "country"=>$request->country , "skills"=>$request->skills ,"cell_no" => $request->cell ,
            "designation" => $request->designation , "qualification" => $request->qualification,
            "certification" => $request->certification
        ]);

        Notify::success('Record Updated Successfully', 'Profile Updated');
        return redirect()->action('AdministratorController@editProfile');

    }



    public function changePassword(){
        return view('admin.setting-changePassword');
    }

    public function generateReport(){
        return view('admin.generate-report');
    }

    public function siteStatistic(){
        return view('admin.site-statistic');
    }

    public function activeUsers(){
        $userStatus = User::all();
//        $all_users = DB::table('users')->select("users.*")->get();
        return view('admin.active-users',compact('userStatus'));
    }

    public function counselorsDetails(){
//        $all_counselors = User::where(['User_type'=>'counselor'])->get();
        $all_counselors = DB::table('users')
            ->join('counselors','users.id','=','counselors.user_id')
            ->select('users.*' ,'counselors.*')->get();

        return view('admin.users-details-counselors',compact('all_counselors'));
    }

    public function customersDetails(){
        $all_customers = User::where(['user_type'=>'customer'])->get();

//        $all_customers = DB::table('users')->where(['User_type'=>'customer'])->get();
        return view('admin.users-details-customers',compact('all_customers'));
    }

    public function visitors(){
        return view('admin.visitors');
    }

    public function postHandling(){
        return view('admin.post-handling');
    }










    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Administrator  $administrator
     * @return \Illuminate\Http\Response
     */
    public function show(Administrator $administrator)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Administrator  $administrator
     * @return \Illuminate\Http\Response
     */
    public function edit(Administrator $administrator)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Administrator  $administrator
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $this->validate($request,[
            'nickname'=>'min:5','city' => 'required','city'=>'required','tags_1'=>'required',
            'designation'=>'required','qualification'=>'required','certification'=>'required',
            'cell'=> 'required',
        ]);

        User::where('id' ,$id)->update(["nickname" =>$request->nickname , "city" =>$request->city, "country" =>$request->country, "designation"=>$request->designation,
            "qualification"=>$request->qualification,"certification"=>$request->certification,"skills"=>$request->tags_1,"cell_no"=>$request->cell]);

        Session::flash('flash_message', 'Record update successfully!');

        return redirect('/Admin/Setting/Edit-Profile');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Administrator  $administrator
     * @return \Illuminate\Http\Response
     */
    public function destroy(Administrator $administrator)
    {
        //
    }
}
