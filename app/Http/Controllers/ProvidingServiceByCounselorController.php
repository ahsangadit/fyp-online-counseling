<?php

namespace App\Http\Controllers;

use App\Providing_Service_by_Counselor;
use Illuminate\Http\Request;

class ProvidingServiceByCounselorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Providing_Service_by_Counselor  $providing_Service_by_Counselor
     * @return \Illuminate\Http\Response
     */
    public function show(Providing__Service_by__Counselor $providing__Service_by__Counselor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Providing_Service_by_Counselor  $providing_Service_by_Counselor
     * @return \Illuminate\Http\Response
     */
    public function edit(Providing__Service_by__Counselor $providing__Service_by__Counselor)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Providing_Service_by_Counselor  $providing_Service_by_Counselor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Providing__Service_by__Counselor $providing__Service_by__Counselor)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Providing_Service_by_Counselor  $providing_Service_by_Counselor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Providing__Service_by__Counselor $providing__Service_by__Counselor)
    {
        //
    }
}
