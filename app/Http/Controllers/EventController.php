<?php

namespace App\Http\Controllers;

use App\counselor;
use App\Event;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Jleon\LaravelPnotify\Notify;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function addEventView()
    {
        return view("counselor.event-addEvent");
    }




    public function addNewEvent(Request $request){
        $user = Auth::user();

        $this->validate($request,[
            'event_name' => 'required|min:5','event_content'=>'required|min:5|max:2000',
            'event_venue'=>'required','available_seats'=>'min:10'
        ]);

        if(isset($request->event_name) && isset($request->event_content) && isset($request->event_venue) && isset($request->available_seats)){

            $file = $request->file('event_img');
            $filename = $file->getClientOriginalName();
            echo $request->event_name." ".$request->event_type." ".$request->event_content." ".$request->available_seats." ".$request->event_venue." ".$request->reservation_time." ".$filename;
            $user_directory = replaceByDash($user->name);

            $file->move('images/frontend_images/events/counselors/'.$user_directory.'_'.$user->id.'/',$filename);

            $event = event::create([
                'counselor_id'=>$user->id, 'event_name'=>$request->event_name, 'event_type'=> $request->event_type ,
                'event_content'=>$request->event_content,'event_seats'=>$request->available_seats,'event_venue'=>$request->event_venue,
                'event_datetime'=>$request->reservation_time,'event_image'=>$filename]);
            DB::table('counselor_event')->insert(['counselor_id'=>$user->id , 'event_id'=>$event->id]);

            Notify::success('Event Add Successfully', 'Event');
            return redirect()->action('EventController@addEventView')->with('flash_message_add_service','Event add successfully');

        }
        else{
            Notify::danger('Something went wrong.Event not be created', 'Event');
            return redirect()->action('EventController@addEventView');
        }

    }



    public function viewEventView(){
        return view("counselor.event-viewEvent");
    }


    public function editEvent(Request $request){


        if($this->validate($request ,['modal_event_id'=>'required','modal_event_name' => 'required|min:5','modal_event_content'=>'required|min:5|max:2000','modal_event_venue'=>'required'])){

            event::where(['id'=>$request->modal_event_id])->update(['event_name' =>$request->modal_event_name, 'event_content'=>$request->modal_event_content ,
                    'event_type'=>$request->modal_event_type , 'event_seats'=> $request->modal_event_seats , 'event_venue'=>$request->modal_event_venue,
                    'event_datetime'=> $request->modal_event_start_end_date]);

            Notify::success('Event updated successfully','Event');
            return redirect()->action("EventController@viewEventView");

        }else{
            Notify::danger('Something went wrong.Service not be updated','Event');
            return redirect()->action("EventController@viewEventView");
        }

    }

    public function deleteEvent(Request $request){
        echo $request->event_id;

        if(isset($request->event_id)){
            event::find($request->event_id)->delete();
            DB::table('counselor_event')->where(['event_id'=>$request->event_id])->delete();

            Notify::success("Event delete successfully","Event");
            return redirect()->action("EventController@viewEventView");
        }else{
            Notify::danger("Something went wrong.Event not be deleted","Event");
            return  redirect()->action("EventController@viewEventView");
        }

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function show(Event $event)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $event)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Event $event)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function destroy(Event $event)
    {
        //
    }
}
