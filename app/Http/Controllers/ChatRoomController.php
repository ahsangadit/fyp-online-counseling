<?php

namespace App\Http\Controllers;

use App\Chat_Room;
use Illuminate\Http\Request;

class ChatRoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Chat_Room  $chat_Room
     * @return \Illuminate\Http\Response
     */
    public function show(Chat_Room $chat_Room)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Chat_Room  $chat_Room
     * @return \Illuminate\Http\Response
     */
    public function edit(Chat_Room $chat_Room)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Chat_Room  $chat_Room
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Chat_Room $chat_Room)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Chat_Room  $chat_Room
     * @return \Illuminate\Http\Response
     */
    public function destroy(Chat_Room $chat_Room)
    {
        //
    }
}
