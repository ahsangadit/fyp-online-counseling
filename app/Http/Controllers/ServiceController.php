<?php

namespace App\Http\Controllers;

use App\post;
use App\Providing_Service_by_Counselor;
use App\service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Jleon\LaravelPnotify\Notify;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function addServiceView()
    {
        return view("counselor.service-addService");
    }

    public function addNewService(Request $request){

        $user_id = Auth::user()->id;



        if(isset($request->service_name) && isset($request->about_service)){

            $this->validate($request,[
                'service_name' => 'required','about_service'=>'required|max:1000'
            ]);

            $service = service::create(['service_name'=>$request->service_name, 'service_content'=>$request->about_service]);
            DB::table('counselor_service')->insert(['counselor_id'=>$user_id , 'service_id'=>$service->id]);

            Notify::success('Service Add Successfully', 'Service');
            return redirect()->action('ServiceController@addServiceView');
        }else{
            Notify::danger('Something went wrong.Service not be created', 'Service');
            return redirect()->action('ServiceController@addServiceView');
        }



    }


    public function viewServiceView(){
//        ,array(['user_details'=>Auth::user()->id])
        return view("counselor.service-viewService");
    }


    public function deleteService(Request $request){

        $service_id = $request->service_id;

        if(isset($service_id)){

            service::find($service_id)->delete();
            DB::table('counselor_service')->where(['service_id'=>$service_id])->delete();

            Notify::success("Service deleted successfully","Service");
            return redirect()->action("ServiceController@viewServiceView");

        }else{
            Notify::danger("Something went wrong.Service not be deleted","Service");
            return  redirect()->action("CounselorController@profile");
        }

    }

    public function editService(Request $request){

        if($this->validate($request ,['modal_service_name'=>'required|min:3' , 'modal_service_content'=>'required|min:10'])){

            service::where(['id'=>$request->modal_service_id])->update(['service_name'=>$request->modal_service_name , 'service_content'=>$request->modal_service_content]);

            Notify::success('Service data updated successfully','Service');
            return redirect()->action("ServiceController@viewServiceView");

        }else{

//            @foreach($errors->all() as $error)
//                                    <li>
//                                        <p><span class="flat" style="color: red;"> {{$error}}</span></p>
//                                    </li>
//            @endforeach

            Notify::danger('Something went wrong.Service not be updated','Service');
            return redirect()->action("ServiceController@viewServiceView");

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function show(Service $service)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Service $service)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function destroy(Service $service)
    {
        //
    }
}
