<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class GenericController extends Controller
{
    //


    public function checkPassword(Request $request){

        $current_password = $request->current_password;
        $user_detail = Auth::user();

        $data = User::where(["user_type"=>$user_detail->user_type])->first();
        if(Hash::check($current_password,$data->password)){
            echo "true";die;
        }else{
            echo "false";die;
        }

    }

    public function updatePassword(Request $request){

           if($request->isMethod('post')){

               $current_pwd = $request->current_password;
               $role = Auth::user()->user_type;

               if($role == "customer"){

                       $check_pwd = User::where(['email'=>Auth::user()->email])->first();
                       if(Hash::check($current_pwd,$check_pwd->password)){
                           $password = bcrypt($request->new_password);
                           $user_id = Auth::user()->id;
                           User::where(['id'=>$user_id])->update(["password"=>$password]);
                           return redirect('/Customer/Setting/Change-Password')->with("flash_message_success" , "Password updated Successfully !");
                       }else{
                           return redirect('/Customer/Setting/Change-Password')->with("flash_message_error" , "Incorrect current Password");
                       }

               }elseif ($role == "counselor"){

                       $check_pwd = User::where(['email'=>Auth::user()->email])->first();
                       if(Hash::check($current_pwd,$check_pwd->password)){
                           $password = bcrypt($request->new_password);
                           $user_id = Auth::user()->id;
                           User::where(['id'=>$user_id])->update(["password"=>$password]);
                           return redirect('/Counselor/Setting/Change-Password')->with("flash_message_success" , "Password updated Successfully !");
                       }else{
                           return redirect('/Counselor/Setting/Change-Password')->with("flash_message_error" , "Incorrect current Password");
                       }

               }elseif ($role == "administrator"){

                       $check_pwd = User::where(['email'=>Auth::user()->email])->first();
                       if(Hash::check($current_pwd,$check_pwd->password)){
                           $password = bcrypt($request->new_password);
                           $user_id = Auth::user()->id;
                           User::where(['id'=>$user_id])->update(["password"=>$password]);
                           return redirect('/Admin/Setting/Change-Password')->with("flash_message_success" , "Password updated Successfully !");
                       }else{
                           return redirect('/Admin/Setting/Change-Password')->with("flash_message_error" , "Incorrect current Password");
                       }

               }else{

               }
           }
    }


    public function help(){
        return view('help');
    }





}
