<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class _LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function login(Request $request){

        $user = DB::table('users')->select('user_type')->where(['email'=>$request->email])->first();
     
       if($request->isMethod('post')){
var_dump($request->isMethod('post'));
            if(isset($user->user_type)){

                if($user->user_type == "counselor"){

                    if(Auth::attempt(['email'=>$request->email,'password'=>$request->password,'user_type'=>$user->user_type])){
                        return redirect('/Counselor/Portal');
                    }else{
                        return redirect('Account-Login')->with('flash_message_error' , 'Invalid email and password');
                    }
                }elseif ($user->user_type == "customer"){

                    if(Auth::attempt(['email'=>$request->email,'password'=>$request->password,'user_type'=>$user->user_type])){
                        return redirect('/Customer/Portal');
                    }else{
                        return redirect('Account-Login')->with('flash_message_error' , 'Invalid email and password');
                    }
                }elseif ($user->user_type === "administrator"){

                    if(Auth::attempt(['email'=>$request->email,'password'=>$request->password,'user_type'=>$user->user_type])){
                        return redirect('/Admin/Portal');
                    }else{
                        return redirect('Account-Login')->with('flash_message_error' , 'Invalid email and password');
                    }
                }

            }else{
               return redirect('Account-Login')->with('flash_message_error' , 'User not found');
               
            }
        }
        return view('Login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
