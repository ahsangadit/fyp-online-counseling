<?php

namespace App\Http\Controllers;

use App\Enroll_In_Event;
use Illuminate\Http\Request;

class EnrollInEventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Enroll_In_Event  $enroll_In_Event
     * @return \Illuminate\Http\Response
     */
    public function show(Enroll_In_Event $enroll_In_Event)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Enroll_In_Event  $enroll_In_Event
     * @return \Illuminate\Http\Response
     */
    public function edit(Enroll_In_Event $enroll_In_Event)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Enroll_In_Event  $enroll_In_Event
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Enroll_In_Event $enroll_In_Event)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Enroll_In_Event  $enroll_In_Event
     * @return \Illuminate\Http\Response
     */
    public function destroy(Enroll_In_Event $enroll_In_Event)
    {
        //
    }
}
