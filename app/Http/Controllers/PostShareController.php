<?php

namespace App\Http\Controllers;

use App\counselor;
use App\post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Storage;
use Jleon\LaravelPnotify\Notify;

class PostShareController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }


    public function newPost(Request $request){

        $user = Auth::user();
        $counselor = counselor::find($user->id);

        $file_names[] = '';

        //        $this->validate($request,['post_title'=>'required|min:3',
        //            'post_content'=>'required'
        //        ]);

        $post_title = isset($request->post_title) ? $request->post_title : '';
        $post_content = isset($request->post_content) ? $request->post_content : '';

        if(!empty($request->post_title) || !empty($request->post_content)){

            foreach ($request->file('post_imgs') as $key => $file) {
                $file_names[$key] = $file->getClientOriginalName();
            }

            $name = implode(',', $file_names);
            $post = post::create(['counselor_id'=>$counselor->id,'post_title'=>$request->post_title,'post_content'=>$request->post_content,'post_image'=>$name]);
            DB::table('counselor_post')->insert(['counselor_id'=>$counselor->id,'post_id'=>$post->id]);


            if($request->hasFile('post_imgs')) {
                foreach ($request->file('post_imgs') as $key => $file) {
                    $file_names[$key] = $file->getClientOriginalName();
                    $user_directory = replaceByDash($user->name);
                    $file->move('images/frontend_images/posts/counselors/'.$user_directory.'_'.$user->id.'/post_id_'.$post->id.'/',$file_names[$key]);
                }
            }

            Notify::success('Post shared successfully','Post');
            return redirect()->action("CounselorController@profile");

        }else{

            Notify::danger('Something went wrong.Post not be created','Post');
            return redirect()->action("CounselorController@profile");

        }

    }


    public function singlePost(Request $request){

        $post = post::find($request->post_id);

        $user_id = $request->post_author_id;

        $user = Auth::user();

        return view('singlePost-page',array('post'=>$post,'user'=>$user,'user_id'=>$user_id));
    }


    public function deletePost(Request $request){

        $user = Auth::user();
        $user_directory = replaceByDash($user->name);
        $post_id = $request->post_id;
        $post = post::where(['id'=>$post_id])->first();

        if(isset($post_id)){
            post::find($post_id)->delete();
            DB::table('counselor_post')->where(['post_id'=>$post_id])->delete();

            File::deleteDirectory('images/frontend_images/posts/counselors/'.$user_directory.'_'.$user->id.'/post_id_'.$post_id);

            Notify::success("Post delete successfully","Post");
            return redirect()->action("CounselorController@profile");

        }else{
            Notify::danger("Something went wrong.Post not be deleted","Post");
           return  redirect()->action("CounselorController@profile");
        }

    }



    public function editPost(Request $request){

        $counselor = counselor::where(['user_id'=>Auth::user()->id])->first();

        $post_title = isset($request->post_title) ? $request->post_title : '';
        $post_content = isset($request->post_content) ? $request->post_content : '';
        $post_id = isset($request->post_id) ? $request->post_id : '';

        //echo $post_content."=".$post_title."=".$post_id;

        if(!empty($post_title) || !empty($post_content)){

            //            foreach ($request->file('post_imgs') as $key => $file) {
            //                $file_names[$key] = $file->getClientOriginalName();
            //            }

            //            $name = implode(',', $file_names);


              $post = post::where(['id'=>$post_id])->update(['counselor_id'=>$counselor->id,'post_title'=>$post_title,'post_content'=>$post_content]);

            //            if($request->hasFile('post_imgs')) {
            //                foreach ($request->file('post_imgs') as $key => $file) {
            //                    $file_names[$key] = $file->getClientOriginalName();
            //                    $user_directory = replaceByDash($user->name);
            //                    $file->move('images/frontend_images/posts/counselors/'.$user_directory.'_'.$user->id.'/post_id_'.$post->id.'/',$file_names[$key]);
            //                }
            //            }

            Notify::success('Post shared successfully','Post');
            return redirect('/Counselor/SinglePost?post_id='.$post_id);

        }else{

            Notify::danger('Something went wrong.Post not be created','Post');
            return redirect()->action("PostShareController@singlePost");

        }
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post_Share  $post_Share
     * @return \Illuminate\Http\Response
     */
    public function show(Post_Share $post_Share)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post_Share  $post_Share
     * @return \Illuminate\Http\Response
     */
    public function edit(Post_Share $post_Share)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post_Share  $post_Share
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post_Share $post_Share)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post_Share  $post_Share
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post_Share $post_Share)
    {
        //
    }
}
