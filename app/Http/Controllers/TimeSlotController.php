<?php

namespace App\Http\Controllers;

use App\Time_Slot;
use Illuminate\Http\Request;

class TimeSlotController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Time_Slot  $time_Slot
     * @return \Illuminate\Http\Response
     */
    public function show(Time_Slot $time_Slot)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Time_Slot  $time_Slot
     * @return \Illuminate\Http\Response
     */
    public function edit(Time_Slot $time_Slot)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Time_Slot  $time_Slot
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Time_Slot $time_Slot)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Time_Slot  $time_Slot
     * @return \Illuminate\Http\Response
     */
    public function destroy(Time_Slot $time_Slot)
    {
        //
    }
}
