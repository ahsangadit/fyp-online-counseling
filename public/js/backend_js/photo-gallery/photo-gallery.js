$(document).ready(function(){        
	$('li.li-gallery img.img-gallery').on('click',function(){
		var src = $(this).attr('src');
		var img = '<img  src="' + src + '" class="img-responsive"/>';
		
		//start of new code new code
		var index = $(this).parent('li.li-gallery').index();
		
		var html = '';
		html += img;                
		html += '<div style="height:25px;clear:both;display:block;">';
		html += '<a class="controls next next-gallery" href="'+ (index+2) + '">next &raquo;</a>';
		html += '<a class="controls previous previous-gallery" href="' + (index) + '">&laquo; prev</a>';
		html += '</div>';
		
		$('#myModal').modal();
		$('#myModal').on('shown.bs.modal', function(){
			$('#myModal .modal-body-gallery').html(html);
			//new code
			$('a.controls').trigger('click');
		})
		$('#myModal').on('hidden.bs.modal', function(){
			$('#myModal .modal-body-gallery').html('');
		});
		
		
		
		
   });	
});
        
         
$(document).on('click', 'a.controls', function(){
	var index = $(this).attr('href');
	var src = $('ul.ul-gallery li.li-gallery:nth-child('+ index +') img.img-gallery').attr('src');
	
	$('.modal-body-gallery img.img-gallery').attr('src', src);
	
	var newPrevIndex = parseInt(index) - 1; 
	var newNextIndex = parseInt(newPrevIndex) + 2; 
	
	if($(this).hasClass('previous')){               
		$(this).attr('href', newPrevIndex); 
		$('a.next').attr('href', newNextIndex);
	}else{
		$(this).attr('href', newNextIndex); 
		$('a.previous').attr('href', newPrevIndex);
	}
	
	var total = $('ul.ul-gallery li.li-gallery').length + 1;
	//hide next button
	if(total === newNextIndex){
		$('a.next').hide();
	}else{
		$('a.next').show()
	}            
	//hide previous button
	if(newPrevIndex === 0){
		$('a.previous').hide();
	}else{
		$('a.previous').show()
	}
	
	
	return false;
});