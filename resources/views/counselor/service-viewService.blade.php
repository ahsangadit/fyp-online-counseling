@extends('layouts.counselor_layout.counselor_design')


@section('title')
    Your Services
@endsection

@section('css-content')
    <!-- Bootstrap -->
    <link href="{{ asset("vendors/backend_vendors/bootstrap/dist/css/bootstrap.min.css")}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ asset("vendors/backend_vendors/font-awesome/css/font-awesome.min.css")}}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{ asset("vendors/backend_vendors/nprogress/nprogress.css")}}" rel="stylesheet">
    <!-- iCheck -->
    <link href="{{ asset("vendors/backend_vendors/iCheck/skins/flat/green.css")}}" rel="stylesheet">

    <!-- bootstrap-progressbar -->
    <link href="{{ asset("vendors/backend_vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css")}}" rel="stylesheet">
    <!-- JQVMap -->
    <link href="{{ asset("vendors/backend_vendors/jqvmap/dist/jqvmap.min.css")}}" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="{{ asset("vendors/backend_vendors/bootstrap-daterangepicker/daterangepicker.css")}}" rel="stylesheet">

    <!-- Datatables -->
    <link href="{{ asset("vendors/backend_vendors/datatables.net-bs/css/dataTables.bootstrap.min.css")}}" rel="stylesheet">
    <link href="{{ asset("vendors/backend_vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.")}}" rel="stylesheet">
    <link href="{{ asset("vendors/backend_vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css")}}" rel="stylesheet">
    <link href="{{ asset("vendors/backend_vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css")}}" rel="stylesheet">
    <link href="{{ asset("vendors/backend_vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css")}}" rel="stylesheet">
    <!-- PNotify -->
    <link href="{{ asset("vendors/backend_vendors/pnotify/dist/pnotify.css")}}" rel="stylesheet">
    <link href="{{ asset("vendors/backend_vendors/pnotify/dist/pnotify.buttons.css")}}" rel="stylesheet">
    <link href="{{ asset("vendors/backend_vendors/pnotify/dist/pnotify.nonblock.css")}}" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="{{ asset("build/backend_build/css/custom.min.css")}}" rel="stylesheet">
@endsection



@section('content')
<?php


use App\Counselor;
use Illuminate\Support\Facades\Auth;
?>
@isset($services)
$services = \App\Counselor::find(Auth::user()->id)->services;
@endisset



    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Your Services</h3>
                </div>
            </div>
            <div class="clearfix"></div>
            {{--<a href="#" service_id="" class="btn btn-danger delete_link" id="delete_link">Delete</a>--}}
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Details</h2>
                            @if (Session::has('notifier.notice'))
                                <script>
                                    new PNotify({!! Session::get('notifier.notice') !!});
                                </script>
                            @endif
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>

                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                <thead>
                                <?php

                                ?>
                                <tr>
                                    <th>ID:</th>
                                    <th>Service:</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                               @isset($services)
                                @if($services)
                                    @foreach($services as $key => $service)
                                        <tr>
                                            <td>{{$key+1}} </td>
                                            <td>{{$service->service_name}}</td>
                                            <td><input type="button" value="Edit" service_id="{{$service->id}}" service_name="{{$service->service_name}}" service_content="{{$service->service_content}}" class="btn btn-primary btn-sm edit_link"></td>
                                            <td><input type="button" value="Delete" class="btn btn-danger btn-sm delete_link" service_id="{{$service->id}}"></td>
                                        </tr>
                                    @endforeach
                                @endif
                                @endisset
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="control-group">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="">
                            <ul class="to_do" style="margin-top: 10px;">
                                @foreach($errors->all() as $error)
                                    <li>
                                        <p><span class="flat" style="color: red;"> {{$error}}</span></p>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>

                {{--Update Service content--}}
                <div class="modal fade" id="editServiceModal" tabindex="-1" role="dialog" aria-labelledby="editServiceModal" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title" id="myModalLabel">Edit Service</h4>
                            </div>
                            <form method="post" data-parsley-validate action="{{url('/Counselor/EditService')}}" enctype="multipart/form-data">
                                <div class="modal-body">
                                    <label class="">Title: </label>
                                    {{csrf_field()}}
                                    <input type="hidden" id="modal_service_id" name="modal_service_id" value="">
                                    <input type="text" id="modal_service_name" name="modal_service_name" class="form-control" value="{" >
                                    <br>
                                    <label class="">Content: </label>
                                    <textarea rows="5" class="resizable_textarea form-control" id="modal_service_content" name="modal_service_content" style="max-width: 100%;" placeholder="Enter your service content..."></textarea>
                                    <br>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="submit" name="post_btn" class="btn btn-success">Update</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>



                {{--Delete Service Modal--}}
                {{--data-toggle="modal" data-target="#basicModal"--}}
                <div class="modal fade" id="deleteServiceModal" tabindex="-1" role="dialog" aria-labelledby="deleteServiceModal" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-bullhorn"></i> Alert</h4>
                            </div>
                            <form method="post" id="delete-modal-form">
                                <div class="modal-body">
                                    <label class="">You want to sure delete this service ?</label>
                                    {{csrf_field()}}
                                </div>
                                <div class="modal-footer">
                                    <a type="submit" class="btn btn-danger modal_delete_link" href="">Yes</a>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>






            </div>
        </div>
    </div>
    <!-- /page content -->




@endsection

@section('js-content')

    {{--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>--}}

    <!-- jQuery -->
    <script src="{{ asset("vendors/backend_vendors/jquery/dist/jquery.min.js")}}"></script>
    <!-- Bootstrap -->
    <script src="{{ asset("vendors/backend_vendors/bootstrap/dist/js/bootstrap.min.js")}}"></script>
    <!-- FastClick -->
    <script src="{{ asset("vendors/backend_vendors/fastclick/lib/fastclick.js")}}"></script>
    <!-- NProgress -->
    <script src="{{ asset("vendors/backend_vendors/nprogress/nprogress.js")}}"></script>
    <!-- Datatables -->
    <script src="{{ asset("vendors/backend_vendors/datatables.net/js/jquery.dataTables.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/datatables.net-bs/js/dataTables.bootstrap.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/datatables.net-buttons/js/dataTables.buttons.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/datatables.net-buttons/js/buttons.flash.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/datatables.net-buttons/js/buttons.html5.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/datatables.net-buttons/js/buttons.print.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/datatables.net-keytable/js/dataTables.keyTable.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/datatables.net-responsive/js/dataTables.responsive.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/datatables.net-scroller/js/dataTables.scroller.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/jszip/dist/jszip.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/pdfmake/build/pdfmake.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/pdfmake/build/vfs_fonts.js")}}"></script>
    <!-- pnotify -->
    <script src="{{ asset("vendors/backend_vendors/pnotify/dist/pnotify.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/pnotify/dist/pnotify.buttons.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/pnotify/dist/pnotify.nonblock.js")}}"></script>
    @include('laravelPnotify::notify')

    <!-- Custom Theme Scripts -->
    <script src="{{ asset("build/backend_build/js/custom.min.js")}}"></script>

    <script>
        $(document).ready(function () {

            $('.edit_link').on('click',function(){
                var service_id = $(this).attr('service_id');
                var service_name = $(this).attr('service_name');
                var service_content = $(this).attr('service_content');
                //alert(service_id+'='+service_name+'='+service_content);

                $("#modal_service_id").attr("value",service_id);
                $("#modal_service_name").attr("value",service_name);
                $("#modal_service_content").text(service_content);
                $("#editServiceModal").modal('show');

            });

            $('.delete_link').on('click' , function(){
                var id = $(this).attr("service_id");
                $('.modal_delete_link').attr("href","{{ url('/Counselor/DeleteService?service_id') }}" + "=" + id);
                $("#deleteServiceModal").modal('show');
            });


        });



    </script>



@endsection
