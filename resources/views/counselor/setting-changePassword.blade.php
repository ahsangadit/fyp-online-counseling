@extends('layouts.counselor_layout.counselor_design')

@section('title')
    Change Password
@endsection

@section('css-content')

    <!-- Bootstrap -->
    <link href="{{ asset("vendors/backend_vendors/bootstrap/dist/css/bootstrap.min.css")}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ asset("vendors/backend_vendors/font-awesome/css/font-awesome.min.css")}}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{ asset("vendors/backend_vendors/nprogress/nprogress.css")}}" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="{{ asset("build/backend_build/css/custom.min.css")}}" rel="stylesheet">

@endsection

@section('content')

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Setting</h3>
                </div>

            </div>
            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Change Password</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            @if(Session::has('flash_message_error'))
                                <div class="alert alert-danger alert-dismissible fade in" role="alert" style="text-align: left;">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                                    </button>
                                    {!! session('flash_message_error') !!}
                                </div>
                            @endif

                            @if(Session::has('flash_message_success'))
                                <div class="alert alert-success alert-dismissible fade in" role="alert" style="text-align: left;">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                                    </button>
                                    {!! session('flash_message_success') !!}
                                </div>
                            @endif

                            <form class="form-horizontal form-label-left" data-parsley-validate  method="post" action="{{url('/Setting/Update-Password')}}">
                                <span class="section">Personal Info</span>
                                <div class="item form-group">
                                    <label for="password" class="control-label col-md-3">Current Password</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input id="current_password" type="password" name="current_password" data-validate-length="6,8" class="form-control col-md-7 col-xs-12" required="required">
                                        {{csrf_field()}}
                                        <span id="check_password"></span>
                                    </div>
                                </div>

                                <div class="item form-group">
                                    <label for="password" class="control-label col-md-3">New Password</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input id="new_password" type="password" name="new_password" data-validate-length="3,8" class="form-control col-md-7 col-xs-12" required="required">
                                    </div>
                                </div>
                                <div class="item form-group">
                                    <label for="password2" class="control-label col-md-3 col-sm-3 col-xs-12">Repeat Password</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input id="password2" type="password" name="password2" data-parsley-equalto="#new_password" class="form-control col-md-7 col-xs-12" required="required">
                                    </div>
                                </div>

                                <div class="ln_solid"></div>
                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-3">
                                        <button id="submit" type="submit" class="btn btn-success">Update Password</button>
                                        <button  class="btn btn-primary">Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->

@endsection



@section('js-content')

    <!-- jQuery -->
    <script src="{{ asset("vendors/backend_vendors/jquery/dist/jquery.min.js")}}"></script>
    <!-- Bootstrap -->
    <script src="{{ asset("vendors/backend_vendors/bootstrap/dist/js/bootstrap.min.js")}}"></script>
    <!-- FastClick -->
    <script src="{{ asset("vendors/backend_vendors/fastclick/lib/fastclick.js")}}"></script>
    <!-- NProgress -->
    <script src="{{ asset("vendors/backend_vendors/nprogress/nprogress.js")}}"></script>
    <!-- validator -->
    <script src="{{ asset("vendors/backend_vendors/parsleyjs/dist/parsley.min.js")}}"></script>
    <!-- Custom Theme Scripts -->
    <script src="{{ asset("build/backend_build/js/custom.min.js")}}"></script>
    <!-- Custom FrontEnd vendor validator -->
    <script src="{{asset("vendors/frontend_vendors/run_time_validation_ajax.js")}}"></script>



@endsection