@extends('layouts.counselor_layout.counselor_design')


@section('title')
    Add Service
@endsection


@section('css-content')

    <!-- Bootstrap -->
    <link href="{{ asset("vendors/backend_vendors/bootstrap/dist/css/bootstrap.min.css")}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ asset("vendors/backend_vendors/font-awesome/css/font-awesome.min.css")}}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{ asset("vendors/backend_vendors/nprogress/nprogress.css")}}" rel="stylesheet">
    <!-- Dropzone.js -->
    <link href="{{ asset("vendors/backend_vendors/dropzone/dist/min/dropzone.min.css")}}" rel="stylesheet">
    <!-- iCheck -->
    <link href="{{ asset("vendors/backend_vendors/iCheck/skins/flat/green.css")}}" rel="stylesheet">
    <!-- bootstrap-wysiwyg -->
    <link href="{{ asset("vendors/backend_vendors/google-code-prettify/bin/prettify.min.css")}}" rel="stylesheet">
    <!-- Select2 -->
    <link href="{{ asset("vendors/backend_vendors/select2/dist/css/select2.min.css")}}" rel="stylesheet">
    <!-- Switchery -->
    <link href="{{ asset("vendors/backend_vendors/switchery/dist/switchery.min.css")}}" rel="stylesheet">
    <!-- starrr -->
    <link href="{{ asset("vendors/backend_vendors/starrr/dist/starrr.css")}}" rel="stylesheet">

    <!-- bootstrap-daterangepicker -->
    <link href="{{ asset("vendors/backend_vendors/bootstrap-daterangepicker/daterangepicker.css")}}"  rel="stylesheet">
    <!-- PNotify -->
    <link href="{{ asset("vendors/backend_vendors/pnotify/dist/pnotify.css")}}" rel="stylesheet">
    <link href="{{ asset("vendors/backend_vendors/pnotify/dist/pnotify.buttons.css")}}" rel="stylesheet">
    <link href="{{ asset("vendors/backend_vendors/pnotify/dist/pnotify.nonblock.css")}}" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="{{ asset("build/backend_build/css/custom.min.css")}}" rel="stylesheet">

@endsection

@section('content')
    <?php
//    use App\Counselor;
//    $user_further_detail = Counselor::where(["User_id"=> $user_detail->id])->first();


    ?>
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Service</h3>
                </div>

            </div>
            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Add Service</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>

                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>

                        @if (Session::has('notifier.notice'))
                            <script>
                                new PNotify({!! Session::get('notifier.notice') !!});
                            </script>
                        @endif
                        <div class="x_content">

                            <p class="font-gray-dark">
                                Using the grid system you can control the position of the labels. The form example below has the <b>col-md-10</b> which sets the width to 10/12 and <b>center-margin</b> which centers it.
                            </p>

                            <form id="demo-form2" class="form-horizontal form-label-left"  method="post" action="{{url('/Counselor/Service/Create-New-Service')}}" >
                                <div class="form-group">
                                    <label class="control-label col-md-2 col-sm-3 col-xs-12">Service Name</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        {{csrf_field()}}
                                        <input type="text" id="service_name" name="service_name" class="form-control" placeholder="Enter service name.."  required="required">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-2 col-sm-3 col-xs-12">About Service</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea rows="5" cols="30" id="about_service" name="about_service" class="form-control" placeholder="Service brief..." required="required" ></textarea>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <hr>
                                        <button type="submit" class="btn btn-success">Add</button>
                                        <button class="btn btn-primary" type="reset">Reset</button>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="">
                                            <ul class="to_do" style="margin-top: 10px;">
                                                @foreach($errors->all() as $error)
                                                    <li>
                                                        <p><span class="flat" style="color: red;"> {{$error}}</span></p>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                            </form>


                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
    <!-- /page content -->

@endsection



@section('js-content')
    <!-- jQuery -->
    <script src="{{ asset("vendors/backend_vendors/jquery/dist/jquery.min.js")}}"></script>
    <!-- Bootstrap -->
    <script src="{{ asset("vendors/backend_vendors/bootstrap/dist/js/bootstrap.min.js")}}"></script>
    <!-- FastClick -->
    <script src="{{ asset("vendors/backend_vendors/fastclick/lib/fastclick.js")}}"></script>
    <!-- NProgress -->
    <script src="{{ asset("vendors/backend_vendors/nprogress/nprogress.js")}}"></script>
    <!-- Dropzone.js -->
    <script src="{{ asset("vendors/backend_vendors/dropzone/dist/min/dropzone.min.js")}}"></script>
    <!-- bootstrap-progressbar -->
    <script src="{{ asset("vendors/backend_vendors/bootstrap-progressbar/bootstrap-progressbar.min.js")}}"></script>
    <!-- iCheck -->
    <script src="{{ asset("vendors/backend_vendors/iCheck/icheck.min.js")}}"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="{{ asset("vendors/backend_vendors/moment/min/moment.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/bootstrap-daterangepicker/daterangepicker.js")}}"></script>
    <!-- bootstrap-wysiwyg -->
    <script src="{{ asset("vendors/backend_vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js")}}"></script>
    <!-- jQuery-hotkeys -->
    <script src="{{ asset("vendors/backend_vendors/jquery.hotkeys/jquery.hotkeys.js")}}"></script>
    <!-- google-code-prettify -->
    <script src="{{ asset("vendors/backend_vendors/google-code-prettify/src/prettify.js")}}"></script>
    <!-- jQuery Tags Input -->
    <script src="{{ asset("vendors/backend_vendors/jquery.tagsinput/src/jquery.tagsinput.js")}}"></script>
    <!-- Switchery -->
    <script src="{{ asset("vendors/backend_vendors/switchery/dist/switchery.min.js")}}"></script>
    <!-- Select2 -->
    <script src="{{ asset("vendors/backend_vendors/select2/dist/js/select2.full.min.js")}}"></script>
    <!-- Parsley -->
    <script src="{{ asset("vendors/backend_vendors/parsleyjs/dist/parsley.min.js")}}"></script>
    <!-- devbridge-autocomplete -->
    <script src="{{ asset("vendors/backend_vendors/devbridge-autocomplete/dist/jquery.autocomplete.js")}}"></script>
    <!-- starrr -->
    <script src="{{ asset("vendors/backend_vendors/starrr/dist/starrr.js")}}"></script>
    <!-- pnotify -->
    <script src="{{ asset("vendors/backend_vendors/pnotify/dist/pnotify.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/pnotify/dist/pnotify.buttons.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/pnotify/dist/pnotify.nonblock.js")}}"></script>
    @include('laravelPnotify::notify')
    <!-- Custom Theme Scripts -->
    <script src="{{ asset("build/backend_build/js/custom.min.js")}}"></script>

@endsection
