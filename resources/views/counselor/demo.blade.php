
    <!-- Bootstrap -->
    <link href="{{ asset("vendors/backend_vendors/bootstrap/dist/css/bootstrap.min.css")}}" rel="stylesheet">

    <!-- Font Awesome -->
    <link href="{{ asset("vendors/backend_vendors/font-awesome/css/font-awesome.min.css")}}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{ asset("vendors/backend_vendors/nprogress/nprogress.css")}}" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="{{ asset("vendors/backend_vendors/bootstrap-daterangepicker/daterangepicker.css")}}" rel="stylesheet">
    <!-- Dropzone.js -->
    <link href="{{ asset("vendors/backend_vendors/dropzone/dist/min/dropzone.min.css")}}" rel="stylesheet">
    <!-- Select2 -->
    <link href="{{ asset("vendors/backend_vendors/select2/dist/css/select2.min.css")}}" rel="stylesheet">
    <!-- PNotify -->
    <link href="{{ asset("vendors/backend_vendors/pnotify/dist/pnotify.css")}}" rel="stylesheet">
    <link href="{{ asset("vendors/backend_vendors/pnotify/dist/pnotify.buttons.css")}}" rel="stylesheet">
    <link href="{{ asset("vendors/backend_vendors/pnotify/dist/pnotify.nonblock.css")}}" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="{{ asset("build/backend_build/css/custom.min.css")}}" rel="stylesheet">

    <style>
        /*Custom Bootstrap Post Gallery*/

        ul.ul-gallery {
            padding:0 0 0 0;
            margin:0 0 0 0;
        }
        ul.ul-gallery li.li-gallery {
            list-style:none;
            margin-bottom:25px;
        }
        ul.ul-gallery li.li-gallery img.img-gallery {
            cursor: pointer;
        }
        .modal-body-gallery {
            padding:5px !important;
        }
        .modal-content-gallery {
            border-radius:0;
        }
        .modal-dialog-gallery img.img-gallery {
            text-align:center;
            margin:0 auto;
        }
        .controls{
            width:50px;
            display:block;
            font-size:11px;
            padding-top:8px;
            font-weight:bold;
        }
        .next {
            float:right;
            text-align:right;
        }
        /*override modal for demo only*/
        .modal-dialog-gallery {
            max-width:500px;
            padding-top: 90px;
        }
        @media screen and (min-width: 768px){
            .modal-dialog-gallery {
                width:500px;
                padding-top: 90px;
            }
        }
        @media screen and (max-width:1500px){
            #ads {
                display:none;
            }
        }

    </style>


    <div class="right_col" role="main">
        <div class="">


            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>User Profile <small>Activity report</small></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>

                        <div class="x_content">
                            <div class="col-md-3 col-sm-3 col-xs-12 profile_left">
                                <div class="profile_img">
                                    <div id="crop-avatar">
                                        <!-- Current avatar -->
                                        <img class="img-responsive avatar-view" src="{{setProfilePicture()}}" alt="Avatar" title="Change the avatar">
                                    </div>
                                </div>


                                <a class="btn btn-success" href="{{url('Counselor/Setting/Edit-Profile')}}"><i class="fa fa-edit m-right-xs"></i>Edit Profile</a>
                                <br />

                                <!-- start skills -->
                                <h4>Skills</h4>
                                <ul class="list-unstyled user_data">
                                    <li>
                                        <p>

                                        </p>
                                    </li>
                                </ul>
                                <!-- end of skills -->

                            </div>


                            <div class="col-md-9 col-sm-9 col-xs-12">

                                <div class="" role="tabpanel" data-example-id="togglable-tabs">
                                    <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#tab_content1" role="tab" id="home-tab"  data-toggle="tab" aria-expanded="true">Profile</a>
                                        </li>

                                        <li role="presentation" class=""><a href="#tab_content2" id="timeline-tab2" role="tab" data-toggle="tab" aria-expanded="false">Timeline</a>
                                        </li>

                                        <li role="presentation" class=""><a href="#tab_content3" id="profile-tab3" role="tab" data-toggle="tab" aria-expanded="false">Recent Activity</a>
                                        </li>

                                    </ul>
                                    <div id="myTabContent" class="tab-content">

                                        <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                                            <div class="x_title">
                                                <h2>Summary</h2>
                                                <div class="clearfix"></div>
                                            </div>
                                            <p>  </p>

                                            <!-- Large modal -->
                                            <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target=".bs-example-modal-lg">Update Summary</button>

                                            <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
                                                <div class="modal-dialog modal-lg">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                                                            </button>
                                                            <h4 class="modal-title" id="myModalLabel">Update Summary</h4>
                                                        </div>
                                                        <form method="post" action="{{url('/Counselor/Update-Profile-Details-Summary')}}">
                                                            <div class="modal-body">
                                                                <textarea rows="5" class="resizable_textarea form-control" id="summary" name="summary" style="max-width: 100%;" placeholder="This text area automatically resizes its height as you fill
                                                                in more text courtesy of autosize-master it out..."></textarea>


                                                            </div>
                                                            <div class="modal-footer">
                                                                {{csrf_field()}}
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                <button type="submit" class="btn btn-success">Save changes</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div role="tabpanel" class="tab-pane fade " id="tab_content2" aria-labelledby="timeline-tab">
                                            <!-- start recent activity -->
                                            <div class="x_title">
                                                <h2>Posts</h2>

                                                <div class="clearfix"></div>
                                                <!-- Large modal -->
                                                <button type="button" class="btn btn-success btn-sm" id="basisModal-btn" data-toggle="modal" data-target="#basicModal">New Posts</button>
                                                <a href="{{url('demo')}}">Demo</a>
                                                <!-- Post Results-->

                                                <ul class="row ul-gallery">
                                                    <li class="col-lg-2 col-md-2 col-sm-3 col-xs-4 li-gallery">
                                                        <img class="img-responsive img-gallery" width="450" height="350" src="http://myproject.com/LARAVEL/Online_Counseling_System/public/images/backend_images/media.jpg/cropper.jpg">
                                                    </li>
                                                    <li class="col-lg-2 col-md-2 col-sm-3 col-xs-4 li-gallery">
                                                        <img class="img-responsive img-gallery" width="450" height="350" src="http://myproject.com/LARAVEL/Online_Counseling_System/public/images/backend_images/media.jpg">
                                                    </li>
                                                </ul>


                                                <div class="modal fade model-gallery" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog modal-dialog-gallery">
                                                        <div class="modal-content modal-content-gallery">
                                                            <div class="modal-body modal-body-gallery">
                                                            </div>
                                                        </div><!-- /.modal-content -->
                                                    </div><!-- /.modal-dialog -->
                                                </div><!-- /.modal -->




                                                <!-- Post Results End -->


                                            </div>


                                            <div class="modal fade" id="basicModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                            <h4 class="modal-title" id="myModalLabel">New Post</h4>
                                                        </div>
                                                        <form method="post" data-parsley-validate action="{{url('/Counselor/New-Post')}}" enctype="multipart/form-data">
                                                            <div class="modal-body">

                                                                <label class="">Title: </label>
                                                                {{csrf_field()}}
                                                                <input type="text" id="post_title" name="post_title" class="form-control" placeholder="Enter post title..." >
                                                                <br>
                                                                <label class="">Content: </label>
                                                                <textarea rows="5" class="resizable_textarea form-control" id="post_content" name="post_content" style="max-width: 100%;" placeholder="Enter your post content..."></textarea>
                                                                <br>
                                                                <label class="">Upload pictures: </label>
                                                                <div class="input-group">
                                                                    <label class="input-group-btn">
                                                                                <span class="btn btn-success">
                                                                                    Browse&hellip; <input type="file" name="post_imgs[]" id="post_imgs" class="form-control " style="display: none;" multiple>
                                                                                </span>
                                                                    </label>
                                                                    <input type="text" class="form-control" readonly>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">

                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                <button type="submit" name="post_btn" class="btn btn-success">Share</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>



                                        <div role="tabpanel" class="tab-pane fade " id="tab_content3" aria-labelledby="profile-tab">
                                            <!-- start recent activity -->
                                            <ul class="messages">
                                                <li>
                                                    <img src="{{ profile_img_placeholder() }}" class="avatar" alt="Avatar">
                                                    <div class="message_date">
                                                        <h3 class="date text-info">24</h3>
                                                        <p class="month">May</p>
                                                    </div>
                                                    <div class="message_wrapper">
                                                        <h4 class="heading">Desmond Davison</h4>
                                                        <blockquote class="message">Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua butcher retro keffiyeh dreamcatcher synth.</blockquote>
                                                        <br />
                                                        <p class="url">
                                                            <span class="fs1 text-info" aria-hidden="true" data-icon=""></span>
                                                            <a href="#"><i class="fa fa-paperclip"></i> User Acceptance Test.doc </a>
                                                        </p>
                                                    </div>
                                                </li>
                                            </ul>
                                            <!-- end recent activity -->
                                        </div>



                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->



    <!-- jQuery -->
    <script src="{{ asset("vendors/backend_vendors/jquery/dist/jquery.min.js")}}"></script>
    <!-- Bootstrap -->
    <script src="{{ asset("vendors/backend_vendors/bootstrap/dist/js/bootstrap.min.js")}}"></script>
    <!-- FastClick -->
    <script src="{{ asset("vendors/backend_vendors/fastclick/lib/fastclick.js")}}"></script>
    <!-- NProgress -->
    <script src="{{ asset("vendors/backend_vendors/nprogress/nprogress.js")}}"></script>
    <!-- morris -->
    <script src="{{ asset("vendors/backend_vendors/raphael/raphael.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/morris/morris.js")}}"></script>
    <!-- bootstrap-progressbar -->
    <script src="{{ asset("vendors/backend_vendors/bootstrap-progressbar/bootstrap-progressbar.min.js")}}"></script>
    {{--<!-- bootstrap-daterangepicker -->--}}
    {{--<script src="{{ asset("vendors/backend_vendors/bootstrap-daterangepicker/daterangepicker.js")}}"></script>--}}
    {{----}}
    {{--<script src="{{ asset("vendors/backend_vendors/morris/morris.js")}}"></script>--}}
    <!-- dropzone -->
    <script src="{{ asset("vendors/backend_vendors/dropzone/dist/dropzone.js")}}"></script>
    <!-- pnotify -->
    <script src="{{ asset("vendors/backend_vendors/pnotify/dist/pnotify.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/pnotify/dist/pnotify.buttons.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/pnotify/dist/pnotify.nonblock.js")}}"></script>

    <script src="{{ asset("build/backend_build/js/custom.min.js")}}"></script>
    <script src="{{ asset("js/backend_js/photo-gallery/photo-gallery.js")}}"></script>

    <script>
        $(function() {

            // We can attach the `fileselect` event to all file inputs on the page
            $(document).on('change', ':file', function() {
                var input = $(this),
                    numFiles = input.get(0).files ? input.get(0).files.length : 1,
                    label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                input.trigger('fileselect', [numFiles, label]);
            });

            // We can watch for our custom `fileselect` event like this
            $(document).ready( function() {
                $(':file').on('fileselect', function(event, numFiles, label) {

                    var input = $(this).parents('.input-group').find(':text'),
                        log = numFiles > 1 ? numFiles + ' files selected' : label;

                    if( input.length ) {
                        input.val(log);
                    } else {
                        if( log ) alert(log);
                    }

                });
            });

        });
    </script>
