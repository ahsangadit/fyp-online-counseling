@extends('layouts.counselor_layout.counselor_design')


@section('title')
    Your Events
@endsection

@section('css-content')
    <!-- Bootstrap -->
    <link href="{{ asset("vendors/backend_vendors/bootstrap/dist/css/bootstrap.min.css")}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ asset("vendors/backend_vendors/font-awesome/css/font-awesome.min.css")}}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{ asset("vendors/backend_vendors/nprogress/nprogress.css")}}" rel="stylesheet">
    <!-- iCheck -->
    <link href="{{ asset("vendors/backend_vendors/iCheck/skins/flat/green.css")}}" rel="stylesheet">

    <!-- bootstrap-progressbar -->
    <link href="{{ asset("vendors/backend_vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css")}}" rel="stylesheet">
    <!-- JQVMap -->
    <link href="{{ asset("vendors/backend_vendors/jqvmap/dist/jqvmap.min.css")}}" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="{{ asset("vendors/backend_vendors/bootstrap-daterangepicker/daterangepicker.css")}}" rel="stylesheet">

    <!-- Datatables -->
    <link href="{{ asset("vendors/backend_vendors/datatables.net-bs/css/dataTables.bootstrap.min.css")}}" rel="stylesheet">
    <link href="{{ asset("vendors/backend_vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.")}}" rel="stylesheet">
    <link href="{{ asset("vendors/backend_vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css")}}" rel="stylesheet">
    <link href="{{ asset("vendors/backend_vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css")}}" rel="stylesheet">
    <link href="{{ asset("vendors/backend_vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css")}}" rel="stylesheet">

    <!-- PNotify -->
    <link href="{{ asset("vendors/backend_vendors/pnotify/dist/pnotify.css")}}" rel="stylesheet">
    <link href="{{ asset("vendors/backend_vendors/pnotify/dist/pnotify.buttons.css")}}" rel="stylesheet">
    <link href="{{ asset("vendors/backend_vendors/pnotify/dist/pnotify.nonblock.css")}}" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="{{ asset("build/backend_build/css/custom.min.css")}}" rel="stylesheet">
@endsection



@section('content')
    <?php

    use Illuminate\Support\Facades\Auth;
    ?>
    @isset($events)
    $events = \App\Counselor::find(Auth::user()->id)->events;
   @endisset
    


    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Your Services</h3>
                </div>

            </div>
            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Details</h2>
                            @if (Session::has('notifier.notice'))
                                <script>
                                    new PNotify({!! Session::get('notifier.notice') !!});
                                </script>
                            @endif
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>

                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>


                        <div class="control-group">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="">
                                    <ul class="to_do" style="margin-top: 10px;">
                                        @foreach($errors->all() as $error)
                                            <li>
                                                <p><span class="flat" style="color: red;"> {{$error}}</span></p>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="x_content">
                            <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>ID:</th>
                                    <th>Event:</th>
                                    <th>Type:</th>
                                    <th>Venue:</th>
                                    {{--<th>Details:</th>--}}
                                    {{--<th>Seats:</th>--}}
                                    {{--<th>Banner:</th>--}}
                                    <th>Start Date:</th>
                                    <th>End Date:</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                @isset($events)
                                @if($events)
                                    @foreach($events as $key => $event)
                                        <?php
                                          $dates = explode("-",$event->event_datetime);
                                          $user_directory = replaceByDash(Auth::user()->name);
                                        ?>
                                        <tr>
                                            <td>{{$key+1}}</td>
                                            <td>{{$event->event_name}}</td>
                                            <td>{{$event->event_type}}</td>
                                            <td>{{$event->event_venue}}</td>
                                            {{--<td>{{$event->event_content}}</td>--}}
                                            {{--<td>{{$event->event_seats}}</td>--}}
                                            {{--<td><img width="250" height="150" src="{{asset('images/frontend_images/events/counselors/'.$user_directory.'_'.Auth::user()->id.'/'.$event->event_image.'')}}"></td>--}}
                                            <td>{{$dates[0]}}</td>
                                            <td>{{$dates[1]}}</td>
                                            <td><input type="button" value="Edit" class="btn btn-primary btn-sm edit_link"  event_id="{{$event->id}}" event_name="{{$event->event_name}}" event_type="{{$event->event_type}}" available_seats="{{$event->event_seats}}" event_venue="{{$event->event_venue}}" event_start_end_date="{{$event->event_datetime}}" event_content="{{$event->event_content}}" ></td>
                                            <td><input type="button" value="Delete" class="btn btn-danger btn-sm delete_link" event_id="{{$event->id}}"></td>
                                        </tr>
                                    @endforeach
                                @endif
                                @endisset
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>


                {{--Update Service content--}}
                <div class="modal fade" id="editEventModal" tabindex="-1" role="dialog" aria-labelledby="editEventModal" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title" id="myModalLabel">Edit Event Content</h4>
                            </div>
                            <form method="post" data-parsley-validate action="{{url('/Counselor/EditEvent')}}" enctype="multipart/form-data">
                                <div class="modal-body">
                                        <div class="form-group">
                                            <label class=" col-lg-3 col-md-2 col-sm-3 col-xs-12">Available Seats:</label>
                                            <div class="col-lg-3 col-md-2 col-sm-2 col-xs-12">
                                                <input type="number" id="modal_event_seats" name="modal_event_seats" value="" class="form-control"  required="required">
                                            </div>
                                            <label class=" col-xs-12 col-sm-1 col-md-1 col-lg-2">Type:</label>
                                            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-4">
                                                <select class="select2_single form-control" name="modal_event_type" id="modal_event_type">
                                                    <option value="Seminar" >Seminar</option>
                                                    <option value="Workshop" >Workshop</option>
                                                    {{--@if($user_further_detail->functional_area == 'Writer')selected="selected" @endif--}}
                                                </select>
                                            </div>
                                        </div>
                                        <br><br>
                                        <div class="form-group">
                                            <label class="control-label col-lg-3 col-md-2 col-sm-3 col-xs-12">Name:</label>
                                            <div class="col-lg-9 col-md-6 col-sm-6 col-xs-12">
                                                {{csrf_field()}}
                                                <input type="hidden" id="modal_event_id" name="modal_event_id" value="">
                                                <input type="text" id="modal_event_name" name="modal_event_name" class="form-control" placeholder="Enter event name.."  required="required">
                                            </div>
                                        </div>
                                        <br><br>
                                        <div class="control-group">
                                            <label class="control-label col-lg-3 col-md-2 col-sm-3 col-xs-12">Date and Time:</label>
                                            <div class="col-lg-9 col-md-6 col-sm-6 col-xs-12">
                                                <div class="input-prepend input-group">
                                                    <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                                    <input type="text" name="modal_event_start_end_date" id="modal_event_start_end_date" class="form-control" value="01/01/2016 - 01/25/2016" />
                                                </div>
                                            </div>
                                        </div>
                                        <br><br>
                                        <div class="form-group">
                                            <label class="control-label col-lg-3 col-md-2 col-sm-3 col-xs-12">Details:</label>
                                            <div class="col-lg-9 col-md-10 col-sm-6 col-xs-12">
                                                <textarea rows="4" cols="20" id="modal_event_content" name="modal_event_content" style="max-width: 100%;" class="form-control" placeholder="Event brief..." required="required" ></textarea>
                                            </div>
                                        </div>
                                        <br><br><br><br><br><br>
                                        <div class="form-group">
                                            <label class="control-label col-lg-3 col-md-2 col-sm-3 col-xs-12">Venue:</label>
                                            <div class="col-lg-9 col-md-6 col-sm-6 col-xs-12">
                                                <textarea rows="1" cols="20" id="modal_event_venue" name="modal_event_venue" style="max-width: 100%;" class="form-control" placeholder="Venue..." required="required" ></textarea>
                                            </div>
                                        </div>
                                        <br><br>
                                </div>
                                <br>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="submit" name="post_btn" class="btn btn-success">Update</button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>



                {{--Delete Service Modal--}}
                {{--data-toggle="modal" data-target="#basicModal"--}}
                <div class="modal fade" id="deleteEventModal" tabindex="-1" role="dialog" aria-labelledby="deleteEventModal" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-bullhorn"></i> Alert</h4>
                            </div>
                            <form method="post" id="delete-modal-form">
                                <div class="modal-body">
                                    <label class="">You want to sure delete this event ?</label>
                                    {{csrf_field()}}
                                </div>
                                <div class="modal-footer">
                                    <a type="submit" class="btn btn-danger modal_delete_link" href="">Yes</a>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>




            </div>
        </div>
    </div>
    <!-- /page content -->

@endsection

@section('js-content')
    <!-- jQuery -->
    <script src="{{ asset("vendors/backend_vendors/jquery/dist/jquery.min.js")}}"></script>
    <!-- Bootstrap -->
    <script src="{{ asset("vendors/backend_vendors/bootstrap/dist/js/bootstrap.min.js")}}"></script>
    <!-- FastClick -->
    <script src="{{ asset("vendors/backend_vendors/fastclick/lib/fastclick.js")}}"></script>
    <!-- NProgress -->
    <script src="{{ asset("vendors/backend_vendors/nprogress/nprogress.js")}}"></script>
    <!-- bootstrap-progressbar -->
    <script src="{{ asset("vendors/backend_vendors/bootstrap-progressbar/bootstrap-progressbar.min.js")}}"></script>
    <!-- iCheck -->
    <script src="{{ asset("vendors/backend_vendors/iCheck/icheck.min.js")}}"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="{{ asset("vendors/backend_vendors/moment/min/moment.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/bootstrap-daterangepicker/daterangepicker.js")}}"></script>
    <!-- bootstrap-wysiwyg -->
    <script src="{{ asset("vendors/backend_vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js")}}"></script>
    <!-- jQuery-hotkeys -->
    <script src="{{ asset("vendors/backend_vendors/jquery.hotkeys/jquery.hotkeys.js")}}"></script>
    <!-- google-code-prettify -->
    <script src="{{ asset("vendors/backend_vendors/google-code-prettify/src/prettify.js")}}"></script>
    <!-- jQuery Tags Input -->
    <script src="{{ asset("vendors/backend_vendors/jquery.tagsinput/src/jquery.tagsinput.js")}}"></script>
    <!-- Switchery -->
    <script src="{{ asset("vendors/backend_vendors/switchery/dist/switchery.min.js")}}"></script>
    <!-- Select2 -->
    <script src="{{ asset("vendors/backend_vendors/select2/dist/js/select2.full.min.js")}}"></script>
    <!-- Parsley -->
    <script src="{{ asset("vendors/backend_vendors/parsleyjs/dist/parsley.min.js")}}"></script>
    <!-- devbridge-autocomplete -->
    <script src="{{ asset("vendors/backend_vendors/devbridge-autocomplete/dist/jquery.autocomplete.js")}}"></script>
    <!-- starrr -->
    <script src="{{ asset("vendors/backend_vendors/starrr/dist/starrr.js")}}"></script>


    <!-- Datatables -->
    <script src="{{ asset("vendors/backend_vendors/datatables.net/js/jquery.dataTables.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/datatables.net-bs/js/dataTables.bootstrap.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/datatables.net-buttons/js/dataTables.buttons.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/datatables.net-buttons/js/buttons.flash.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/datatables.net-buttons/js/buttons.html5.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/datatables.net-buttons/js/buttons.print.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/datatables.net-keytable/js/dataTables.keyTable.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/datatables.net-responsive/js/dataTables.responsive.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/datatables.net-scroller/js/dataTables.scroller.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/jszip/dist/jszip.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/pdfmake/build/pdfmake.min.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/pdfmake/build/vfs_fonts.js")}}"></script>

    <!-- pnotify -->
    <script src="{{ asset("vendors/backend_vendors/pnotify/dist/pnotify.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/pnotify/dist/pnotify.buttons.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/pnotify/dist/pnotify.nonblock.js")}}"></script>
    @include('laravelPnotify::notify')

    <!-- Custom Theme Scripts -->
    <script src="{{ asset("build/backend_build/js/custom.min.js")}}"></script>

    <script>
        $(document).ready(function () {

            $('.edit_link').on('click',function(){
                var event_id = $(this).attr('event_id');
                var event_name = $(this).attr('event_name');
                var event_type = $(this).attr('event_type');
                var event_venue = $(this).attr('event_venue');
                var event_seats = $(this).attr('available_seats');
                var event_start_end_date = $(this).attr('event_start_end_date');
                var event_content = $(this).attr('event_content');
//                alert(event_type);

                $("#modal_event_id").attr("value",event_id);
                $("#modal_event_name").attr("value",event_name);
                $("#modal_event_type" ).find( 'option[value="' + event_type + '"]' ).attr( "selected", true );
                $("#modal_event_seats").attr("value",event_seats);
                $("#modal_event_start_end_date").attr("value",event_start_end_date);
                $("#modal_event_venue").text(event_venue);
                $("#modal_event_content").text(event_content);
                $("#editEventModal").modal('show');

            });

            $('.delete_link').on('click' , function(){
                var id = $(this).attr("event_id");
                $('.modal_delete_link').attr("href","{{ url('/Counselor/DeleteEvent?event_id') }}" + "=" + id);
                $("#deleteEventModal").modal('show');
            });


        });



    </script>



@endsection
