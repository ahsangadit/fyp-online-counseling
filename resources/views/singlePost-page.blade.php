@extends( Auth::user()->user_type == "counselor" ?  'layouts.counselor_layout.counselor_design' : 'layouts.customer_layout.customer_design' )

@section('title')
{{$post->name}}
@stop

@section('css-content')

    <!-- Bootstrap -->
    <link href="{{ asset("vendors/backend_vendors/bootstrap/dist/css/bootstrap.min.css")}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ asset("vendors/backend_vendors/font-awesome/css/font-awesome.min.css")}}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{ asset("vendors/backend_vendors/nprogress/nprogress.css")}}" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="{{ asset("build/backend_build/css/custom.min.css")}}" rel="stylesheet">

    <style>
        .selected img {
            opacity:0.5;
        }

        @media(min-width: 320px) and (max-width: 767px){
            .carousel-img{
                height: 200px !important;
            }
        }

    </style>
@stop

@section('content')

    <?php

        $user = Auth::user();
        $user_directory = replaceByDash($user->name);


//           if(Auth::user()->user_type == 'counselor' ){
//                $user = Auth::user();
//                $user_directory = replaceByDash($user->name);
//           }elseif(Auth::user()->user_type == 'customer'){
//
//               $user = \App\User::find($user_id);
//               $user_directory = replaceByDash($user->name);
//           }


    ?>
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="clearfix"></div>
            @if (Session::has('notifier.notice'))
                <script>
                    new PNotify({!! Session::get('notifier.notice') !!});
                </script>
            @endif
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            @if($user->user_type == 'counselor')
                                <h2>Post</h2>
                            @else
                                 <h2>{{$user->name}} Post</h2>
                            @endif
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                    @if($user->user_type == 'counselor')
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="#" data-toggle="modal" data-target="#basicModal">Edit Post</a>
                                                </li>
                                            </ul>
                                        </li>
                                    @endif
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>

                            <div class="modal fade" id="basicModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">Edit Post</h4>
                                        </div>
                                        <form method="post" data-parsley-validate action="{{url('/Counselor/EditPost')}}" enctype="multipart/form-data">
                                            <div class="modal-body">
                                                <label class="">Title: </label>
                                                 {{csrf_field()}}
                                                 <input type="hidden" name="post_id" value="{{$post->id}}">
                                                 <input type="text" id="post_title" name="post_title" class="form-control" value="{{$post->post_title}}" >
                                                 <br>
                                                 <label class="">Content: </label>
                                                  <textarea rows="5" class="resizable_textarea form-control" id="post_content" name="post_content" style="max-width: 100%;" placeholder="Enter your post content...">{{$post->post_content}}</textarea>
                                                  <br>
                                                    {{--<label class="">Upload pictures: </label>--}}
                                                    {{--<div class="input-group">--}}
                                                        {{--<label class="input-group-btn">--}}
                                                            {{--<span class="btn btn-success">--}}
                                                                {{--Browse&hellip; <input type="file" name="post_imgs[]" id="post_imgs" class="form-control " style="display: none;" multiple>--}}
                                                            {{--</span>--}}
                                                        {{--</label>--}}
                                                        {{--<input type="text" class="form-control" readonly>--}}
                                                    {{--</div>--}}
                                            </div>
                                            <div class="modal-footer">

                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                <button type="submit" name="post_btn" class="btn btn-success">Update</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="x_content">
                            <div class="container">
                                    <div class="col-md-12">
                                        <h1>{{$post->post_title}}</h1>
                                    </div>
                                    <!-- thumb navigation carousel -->
                                    <?php  $images = explode(',',$post->post_image);  ?>

                                    <div id="myCarousel" class="carousel slide" data-ride="carousel" style="margin-top: 100px">
                                            <!-- Indicators -->
                                            <ol class="carousel-indicators">
                                                @foreach($images as $key => $image)
                                                   <li data-target="#myCarousel" data-slide-to="{{$key}}" class="active"></li>
                                                @endforeach
                                            </ol>
                                            <!-- Wrapper for slides -->
                                            <div class="carousel-inner">
                                                @foreach($images as $key => $image)
                                                    @if($user->user_type == 'counselor')
                                                        <div class="item">
                                                            <img class="carousel-img" src="{{asset('images/frontend_images/posts/counselors/'.$user_directory.'_'.$user->id.'/post_id_'.$post->id.'/'.$image)}}" alt="Image_{{$key}}" style="width: 100% !important;max-height: 400px !important;">
                                                        </div>
                                                    @else
                                                        <div class="item">
                                                            <img class="carousel-img" src="{{asset('images/frontend_images/posts/counselors/Ahsan_1'.'/post_id_'.$post->id.'/'.$image)}}" alt="Image_{{$key}}" style="width: 100% !important;max-height: 400px !important;">
                                                        </div>
                                                    @endif
                                                @endforeach
                                            </div>
                                            <!-- Left and right controls -->
                                            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                                                <span class="glyphicon glyphicon-chevron-left"></span>
                                                <span class="sr-only">Previous</span>
                                            </a>
                                            <a class="right carousel-control" href="#myCarousel" data-slide="next">
                                                <span class="glyphicon glyphicon-chevron-right"></span>
                                                <span class="sr-only">Next</span>
                                            </a>
                                    </div>
                                    <br>

                                    <div class="col-md-12">
                                        <p>{{$post->post_content}}</p>
                                    </div>
                                    <hr>
                                   {{--comment section--}}
                                    <div class="col-md-6">
                                        <div class="x_content">
                                            <h2>Comments</h2>
                                            <ul class="list-unstyled msg_list">
                                                <li>
                                                    <a>
                                                    <span class="image">
                                                      <img src="{{profile_img_placeholder()}}" alt="img" />
                                                    </span>
                                                        <span>
                                                      <span>John Smith</span>
                                                      <span class="time">3 mins ago</span>
                                                    </span>
                                                        <span class="message">
                                                        Add comments to this post ...
                                                    </span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                <div class="col-md-6"></div>



                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->

@stop


@section('js-content')
    <!-- jQuery -->
    <script src="{{ asset("vendors/backend_vendors/jquery/dist/jquery.min.js")}}"></script>
    <!-- Bootstrap -->
    <script src="{{ asset("vendors/backend_vendors/bootstrap/dist/js/bootstrap.min.js")}}"></script>
    <!-- FastClick -->
    <script src="{{ asset("vendors/backend_vendors/fastclick/lib/fastclick.js")}}"></script>
    <!-- NProgress -->
    <script src="{{ asset("vendors/backend_vendors/nprogress/nprogress.js")}}"></script>
    <!-- pnotify -->
    <script src="{{ asset("vendors/backend_vendors/pnotify/dist/pnotify.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/pnotify/dist/pnotify.buttons.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/pnotify/dist/pnotify.nonblock.js")}}"></script>

    @include('laravelPnotify::notify')
    <!-- Custom Theme Scripts -->
    <script src="{{ asset("build/backend_build/js/custom.min.js")}}"></script>

    <script>
        $(document).ready(function () {
            $('#myCarousel').find('.item').first().addClass('active');
        });

    </script>

@stop


