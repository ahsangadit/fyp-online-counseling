<!-- menu profile quick info -->
<div class="profile clearfix">
    <div class="profile_pic">
        <img src="{{setProfilePicture()}}" alt="..." class="img-circle profile_img">

        {{--<img src="@if(isset($user_detail->Profile_img)) {{asset('/images/frontend_images/profile/')}}{{$user_detail->Profile_img}} @else{{profile_img_placeholder()}}@endif" alt="..." class="img-circle profile_img">--}}
    </div>
    <div class="profile_info">
        <span>Welcome,</span>

        <h2>{{ Auth::user()->name }}</h2>

    </div>
</div>
<!-- /menu profile quick info -->