<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="images/favicon.ico" type="image/ico" />

    <title> @yield('title') </title>

    @yield('css-content')
</head>

<body class="nav-md">

<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="#" class="site_title" style="font-size: 20px"><i class="fa fa-paw"></i> <span>{{ web_title() }}</span></a>
                </div>

                    <div class="clearfix"></div>

                    @include('layouts.counselor_layout.counselor_profile_sidebar_info')

                    <br />

                    @include('layouts.counselor_layout.counselor_sidebar')

                    @include('layouts.counselor_layout.counselor_menu_footer_button')

            </div>
        </div>

        @include('layouts.counselor_layout.counselor_header')

        @yield('content')

        @include('layouts.counselor_layout.counselor_footer')
    </div>
</div>


    @yield('js-content')

</body>
</html>
