<!-- sidebar menu -->
<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section">
        <h3>General</h3>
        <ul class="nav side-menu">
            <li><a href="{{url('Counselor/Portal')}}"><i class="fa fa-home"></i>Home</a></li>

            <li><a href="{{url('Counselor/Profile')}}"><i class="fa fa-user"></i>Profile</a></li>
            <li><a><i class="fa fa-cog"></i>Setting <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="{{url('Counselor/Setting/Edit-Profile')}}">Edit Profile</a></li>
                    <li><a href="{{url('Counselor/Setting/Change-Password')}}">Change Password</a></li>
                </ul>

            </li>



            <li><a><i class="fa fa-cogs"></i>Services<span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="{{url('Counselor/Service/Create-Services')}}">Create Services</a></li>
                    <li><a href="{{url('Counselor/Service/View-Services')}}">Show Services</a></li>
                </ul>
            </li>
        </ul>
    </div>
    <div class="menu_section">
        <h3>Extra</h3>
        <ul class="nav side-menu">
            <li><a><i class="fa fa-cubes"></i>Events<span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="{{url('Counselor/Event/Create-Event')}}">Add Seminars</a></li>
                    <li><a href="{{url('Counselor/Event/View-Event')}}">Show Seminars / Workshop</a></li>
                </ul>
            </li>
            <li><a href="{{url('Counselor/Following')}}"><i class="fa fa-sitemap"></i>See Followers</a></li>
        </ul>
    </div>

</div>
<!-- /sidebar menu -->