<!-- sidebar menu -->
<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section">
        <h3>General</h3>
        <ul class="nav side-menu">
            <li><a href="{{url('Customer/Portal')}}"><i class="fa fa-home"></i>Home</a></li>

            <li><a href="{{url('Customer/Profile')}}"><i class="fa fa-user"></i>Profile</a></li>
            <li><a><i class="fa fa-cog"></i>Setting <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="{{url('Customer/Setting/Edit-Profile')}}">Edit Profile</a></li>
                    <li><a href="{{url('Customer/Setting/Change-Password')}}">Change Password</a></li>
                </ul>

            </li>


            <li><a><i class="fa fa-users"></i>Counselling <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="{{url('Customer/Counselling/Search-Counselors')}}">Search Counselors</a></li>
                    <li><a href="{{url('Customer/Counselling/Subscribed-Counselors')}}">Subscribed Counselors</a></li>
                </ul>
            </li>
        </ul>
    </div>
    <div class="menu_section">
        <h3>Extra</h3>
        <ul class="nav side-menu">
            <li><a href="{{url('Customer/Join-Events')}}"><i class="fa fa-cubes"></i>Join Event</a></li>
            {{--<li><a href="{{url('Customer/Following')}}"><i class="fa fa-sitemap"></i>Following</a></li>--}}
        </ul>
    </div>

</div>
<!-- /sidebar menu -->