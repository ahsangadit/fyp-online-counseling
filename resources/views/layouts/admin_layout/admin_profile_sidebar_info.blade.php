<!-- menu profile quick info -->
<div class="profile clearfix">
    <div class="profile_pic">
        <img src="{{setProfilePicture()}}" alt="..."  class="img-circle profile_img">
    </div>
    <div class="profile_info">
        <span>Welcome,</span>
        <h2 style="text-transform: capitalize;">{{Auth::user()->name}}</h2>
    </div>
</div>
<!-- /menu profile quick info -->