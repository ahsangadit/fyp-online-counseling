@extends('layouts.customer_layout.customer_design')


@section('title')
    Search Counselors
@endsection


@section('css-content')

    <!-- Bootstrap -->
    <link href="{{ asset("vendors/backend_vendors/bootstrap/dist/css/bootstrap.min.css")}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ asset("vendors/backend_vendors/font-awesome/css/font-awesome.min.css")}}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{ asset("vendors/backend_vendors/nprogress/nprogress.css")}}" rel="stylesheet">
    <!-- iCheck -->
    <link href="{{ asset("vendors/backend_vendors/iCheck/skins/flat/green.css")}}" rel="stylesheet">

    <link href="{{ asset("vendors/backend_vendors/switchery/dist/switchery.min.css")}}" rel="stylesheet">

    <!-- bootstrap-wysiwyg -->
    <link href="{{ asset("vendors/backend_vendors/google-code-prettify/bin/prettify.min.css")}}" rel="stylesheet">
    <!-- PNotify -->
    <link href="{{ asset("vendors/backend_vendors/pnotify/dist/pnotify.css")}}" rel="stylesheet">
    <link href="{{ asset("vendors/backend_vendors/pnotify/dist/pnotify.buttons.css")}}" rel="stylesheet">
    <link href="{{ asset("vendors/backend_vendors/pnotify/dist/pnotify.nonblock.css")}}" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="{{ asset("build/backend_build/css/custom.min.css")}}" rel="stylesheet">

    <style>

        .x_panel{
            padding: 10px 14px;
        }

        ul.widget_profile_box{
            display: inline-flex;
        }

    </style>

@endsection


@section('content')

    <?php

    use App\counselor;
    use App\customer;
    use App\subscriber;
    use Illuminate\Support\Facades\Session;


    $avail_industry = counselor::groupBy('industry')->selectRaw('count(*) as total , industry')->get();
        $avail_functionalArea = counselor::groupBy('functional_area')->selectRaw('count(*) as total , functional_area')->get();
        $avail_experience = counselor::groupBy('experience')->selectRaw('count(*) as total , experience')->get();


        $customer = customer::where(['user_id'=>Auth::user()->id])->first();
        $subscribers = customer::find($customer->id)->subscribers;

       



    ?>

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Counselling</h3>
                </div>

            </div>
            <div class="clearfix"></div>
            @if (Session::has('notifier.notice'))
                <script>
                    new PNotify({!! Session::get('notifier.notice') !!});
                </script>
            @endif


            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Search Counselors</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left"  method="post" action="{{url('/Customer/Counselling/Searching-Counselors')}}">

                                <div class="form-group">
                                    {{csrf_field()}}
                                    <label class="control-label col-xs-12 col-sm-1 col-md-1 col-lg-3">Industry:</label>
                                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-3">
                                        <select class="select2_single form-control" name="industry" id="event_type">
                                            <option value="Select Industry">Select Industry</option>
                                            @foreach($avail_industry as $key => $value)
                                                <option value="{{$value->industry}}">{{$value->industry}}</option>

                                            @endforeach
                                        </select>
                                    </div>
                                    <label class="control-label col-xs-12 col-sm-3 col-md-3 col-lg-2">Functional Area:</label>
                                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-3">
                                        <select class="select2_single form-control" name="functional_area" id="event_type">
                                            <option value="Select Functional Area">Select Functional Area</option>
                                            @foreach($avail_functionalArea as $key => $value)
                                                <option value="{{$value->functional_area}}">{{$value->functional_area}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-xs-12 col-sm-3 col-md-3 col-lg-3">Experience:</label>
                                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-3">
                                        <select class="select2_single form-control" name="experience" id="event_type">
                                            <option value="Select Experience">Select Experience</option>
                                            @foreach($avail_experience as $key => $value)
                                                <option value="{{$value->experience}}">{{$value->experience}} Exp</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <label class="control-label col-xs-12 col-sm-1 col-md-1 col-lg-2">Gender:</label>
                                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-3" style="display: inline-flex">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="gender_m" class="flat"> Males
                                            </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="gender_f" class="flat"> Females
                                            </label>
                                        </div>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <div class="col-md-12 col-sm-12 col-xs-12" style="text-align: center">
                                        <button type="submit" class="btn btn-success">Filter</button>
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="control-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="">
                                            <ul class="to_do" style="margin-top: 10px;">
                                                @foreach($errors->all() as $error)
                                                    <li>
                                                        <p><span class="flat" style="color: red;"> {{$error}}</span></p>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                            </form>

                            <hr>

                                <?php

                                    if (Session::has('results')){
                                        $results = Session::get('results');
                                        foreach ($results as $key => $counselor){
//                                custom_printR($results);
                                ?>

                                <!-- Profile Design Start -->
                                    <div class="col-md-3 col-xs-12 widget widget_tally_box">
                                        <div class="x_panel fixed_height_390" style="height: 100%;">
                                            <div class="x_content">
                                                <div class="flex">
                                                    <ul class="list-inline widget_profile_box">
                                                        <li>
                                                            <a href="#">
                                                                <i class="fa fa-facebook"></i>
                                                            </a>
                                                        </li>
                                                        <li>
                                                        <?php
                                                            $user_directory = replaceByDash($counselor->name);
                                                        ?>
                                                            {{--{{asset('images/frontend_images/profile/counselors/'.$user_directory.'_'.$counselor->id.'/'.$counselor->profile_img)}}--}}
                                                        <img src="{{asset('images/frontend_images/profile/counselors/'.$user_directory.'_'.$counselor->User_ID.'/'.$counselor->profile_img)}}" alt="..." class="img-circle profile_img">

                                                        </li>
                                                        <li>
                                                            <a href="#">
                                                                <i class="fa fa-linkedin"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>

                                                <h3 class="name"><a href="{{url('/Customer/CounselorProfile?name='.$counselor->name.'&user_id='.$counselor->User_ID)}}">{{$counselor->name}}</a></h3>

                                                <div class="flex">
                                                    <ul class="list-inline count2">
                                                        <li>
                                                            <h3>1</h3>
                                                            <span>Posts</span>
                                                        </li>
                                                        <li>
                                                            <h3>2</h3>
                                                            <span>Followers</span>
                                                        </li>
                                                        <li>
                                                            <h3>3</h3>
                                                            <span>Events</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <p><strong>Industry</strong></p>
                                                <p>{{$counselor->industry}}</p>
                                                <p><strong>Functional Area</strong></p>
                                                <p>{{$counselor->functional_area}}</p>
                                                <p><strong>Experience</strong></p>
                                                <p>{{$counselor->experience}}</p>
                                            </div>

                                            <form action="" method="post">
                                                <div class="form-group">
                                                    <label class="control-label col-md-6 col-sm-6 col-xs-6" style="text-align: right;">Follow</label>
                                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                                        <div class="">
                                                            {{csrf_field()}}

                                                            <label><input type="checkbox" id="follow_{{$counselor->id}}" customer="{{$customer->id}}" counselor="{{$counselor->id}}" name="follow" class="js-switch" @foreach($subscribers as $subscriber) @if($subscriber->counselor_id == $counselor->id) {{$subscriber->attribute}} @endif  @endforeach/></label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>

                                        </div>
                                    </div> <!-- Profile Design End -->

                                <?php
                                        }

                                    }
                                ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->

@endsection



@section('js-content')

    <!-- jQuery -->
    <script src="{{ asset("vendors/backend_vendors/jquery/dist/jquery.min.js")}}"></script>
    <!-- Bootstrap -->
    <script src="{{ asset("vendors/backend_vendors/bootstrap/dist/js/bootstrap.min.js")}}"></script>
    <!-- FastClick -->
    <script src="{{ asset("vendors/backend_vendors/fastclick/lib/fastclick.js")}}"></script>
    <!-- NProgress -->
    <script src="{{ asset("vendors/backend_vendors/nprogress/nprogress.js")}}"></script>
    <!-- iCheck -->
    <script src="{{ asset("vendors/backend_vendors/iCheck/icheck.min.js")}}"></script>
    <!-- Switchery -->
    <script src="{{ asset("vendors/backend_vendors/switchery/dist/switchery.min.js")}}"></script>
    <!-- validator -->
    <script src="{{ asset("vendors/backend_vendors/parsleyjs/dist/parsley.min.js")}}"></script>

    <!-- pnotify -->
    <script src="{{ asset("vendors/backend_vendors/pnotify/dist/pnotify.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/pnotify/dist/pnotify.buttons.js")}}"></script>
    <script src="{{ asset("vendors/backend_vendors/pnotify/dist/pnotify.nonblock.js")}}"></script>
    @include('laravelPnotify::notify')
    <!-- Custom Theme Scripts -->
    <script src="{{ asset("build/backend_build/js/custom.min.js")}}"></script>
    <script>

        $(document).ready(function () {

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });



                //======================================================================================
                $('input:checkbox[id^="follow_"]').on('click',function(){

                    var customer_id = $(this).attr("customer");
                    var counselor_id = $(this).attr("counselor");

                    if($(this).is(':checked')){
                        //alert("Checked"+' '+$(this).attr("id")+' '+customer_id+' '+counselor_id);

                        $.ajax({
                            type:'get',
                            url:'http://myproject.com/LARAVEL/Online_Counseling_System/public/Customer/Subscribe',
                            data:{customer_id:customer_id,counselor_id:counselor_id},
                            success:function(response){
                                if(response == 'true'){
                                  //  alert('Checked true');
                                }
                            },
                            error:function(response){
                                if(response == 'false'){
                                  //  alert('Checked false');
                                }
                            }
                        });

                    }else if($('input:checkbox[id^="follow_"]:checked')){

                        //     alert("Unchecked"+' '+$(this).attr("id")+' '+$(this).attr("customer")+' '+$(this).attr("counselor"));
                        //     $('input:checkbox[id^="follow_"]:unchecked');
                        $.ajax({
                            type:'get',
                            url:'http://myproject.com/LARAVEL/Online_Counseling_System/public/Customer/Unsubscribe',
                            data:{customer_id:customer_id,counselor_id:counselor_id},
                            success:function(response){
                                if(response == 'true'){
                                  //  alert('Unchecked true');
                                    $('input:checkbox[id^="follow_"]:unchecked');
                                }
                            },
                            error:function(response){
                                if(response == 'false'){
                                 //   alert('Unchecked false');
                                }
                            }
                        });


                    }
               });

               //======================================================================================

        });

    </script>
@endsection