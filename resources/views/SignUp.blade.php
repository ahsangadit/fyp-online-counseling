<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf_token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>SignUp |</title>

    <!-- Bootstrap -->
    <link href="{{ asset("vendors/backend_vendors/bootstrap/dist/css/bootstrap.min.css")}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ asset("vendors/backend_vendors/font-awesome/css/font-awesome.min.css")}}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{ asset("vendors/backend_vendors/nprogress/nprogress.css")}}" rel="stylesheet">
    <!-- Dropzone.js -->
    <link href="{{ asset("vendors/backend_vendors/dropzone/dist/min/dropzone.min.css")}}" rel="stylesheet">
    <!-- iCheck -->
    <link href="{{ asset("vendors/backend_vendors/iCheck/skins/flat/green.css")}}" rel="stylesheet">
    <!-- bootstrap-wysiwyg -->
    <link href="{{ asset("vendors/backend_vendors/google-code-prettify/bin/prettify.min.css")}}" rel="stylesheet">
    <!-- Select2 -->
    <link href="{{ asset("vendors/backend_vendors/select2/dist/css/select2.min.css")}}" rel="stylesheet">
    <!-- Switchery -->
    <link href="{{ asset("vendors/backend_vendors/switchery/dist/switchery.min.css")}}" rel="stylesheet">
    <!-- starrr -->
    <link href="{{ asset("vendors/backend_vendors/starrr/dist/starrr.css")}}" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="{{ asset("vendors/backend_vendors/bootstrap-daterangepicker/daterangepicker.css")}}" rel="stylesheet">
    <!-- bootstrap-datetimepicker -->
    <link href="{{ asset("vendors/backend_vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css")}}" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="{{ asset("build/backend_build/css/custom.min.css")}}" rel="stylesheet">

    <style>

        html,
        body {
            height: 100%;
            width: 100%;
            margin: 0;
            padding: 0;
            overflow: hidden;
            overflow-y: scroll;
            position: inherit !important;
        }

        #particle-canvas {
            width: 100%;
            height: 100%;
        }


        .card-style{
            background: #F7F7F7;
            /*padding: 25px;*/
            padding: 25px 20px 0px 20px;
            border: 3px solid #73879C;
            border-radius: 20px;
            z-index: 22;
        }

        .opacity{
            opacity: 1;
        }

        .login_wrapper {
            right: 0;
            margin: 5% auto 0;
            max-width: 900px;
            /* min-height: 500px; */
            position: relative;
        }

        .x_panel {
            position: absolute;
         }

        @media (min-width: 320px) and (max-width: 768px){
            .x_panel {
                position: inherit;
                margin-bottom: -500px;
            }
        }

        @media (min-width: 320px) and (max-width: 450px){
            .separate-link {
                text-align: center;
                margin-top: 0px !important;
            }
            .actionBar {
                text-align: center;
            }
        }

        .wizard_steps{
            padding: 0px 5px 0px 5px;
        }

        .separate-link {
            margin-top: -35px;
        }


    </style>

</head>

<body class="login" id="particle-canvas">
<div>

        <div class="login_wrapper">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel card-style">
                        <div class="x_title">
                            <h2>Create Account</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div id="result"></div>

                        <div class="x_content">


                            









                            <!-- Smart Wizard -->
                            <div id="wizard" class="form_wizard wizard_horizontal">
                                <ul class="wizard_steps">
                                    <li>
                                        <a href="#step-1">
                                            <span class="step_no">1</span>
                                            <span class="step_descr">
                                              Step 1<br />
                                              <small>Personal Info</small>
                                          </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#step-2">
                                            <span class="step_no">2</span>
                                            <span class="step_descr">
                                              Step 2<br />
                                              <small>General Info</small>
                                          </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#step-3">
                                            <span class="step_no">3</span>
                                            <span class="step_descr">
                                              Step 3<br />
                                              <small>Career Info</small>
                                          </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#step-4">
                                            <span class="step_no">4</span>
                                            <span class="step_descr">
                                              Step 4<br />
                                              <small>Career Info</small>
                                          </span>
                                        </a>
                                    </li>
                                </ul>


                                <div id="step-1" class="step">
                                    <form data-parsley-validate class="form-horizontal form-label-left" action="{{url('SignUp/save')}}" method="post" id="regform1">
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Username</label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="username" name="username" class="form-control" placeholder="Name" required="required">
                                                {{$errors->first('username')}}
                                                <input type="hidden" name="_method" value="PUT">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Password</label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="password" id="password" name="password" class="form-control" placeholder="Password">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Nick Name</label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="nickname" name="nickname" class="form-control" placeholder="NickName">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Email</label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="email" name="email" class="form-control" placeholder="Email"  >
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Date Of Birth <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="date" id="dob" name="dob" class="form-control" placeholder="Date of Birth" required="required" >

                                                {{--<fieldset>--}}
                                                    {{--<div class="control-group">--}}
                                                        {{--<div class="controls">--}}
                                                            {{--<div class="col-md-11 xdisplay_inputx form-group has-feedback">--}}
                                                                {{--<input type="text" class="form-control has-feedback-left" id="single_cal4"  aria-describedby="inputSuccess2Status4">--}}
                                                                {{--<span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>--}}
                                                                {{--<span id="inputSuccess2Status4" class="sr-only">(success)</span>--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                {{--</fieldset>--}}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 col-sm-3 col-xs-12" style="text-align: right;">Gender
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                M:
                                                <input type="radio" class="flat" name="gender" id="genderM" value="male" checked="" required /> F:
                                                <input type="radio" class="flat" name="gender" id="genderF" value="female" />

                                            </div>
                                        </div>
                                    </form>
                                </div>


                                <div id="step-2" class="step">
                                    <form class="form-horizontal form-label-left" action="{{url('SignUp/save')}}" method="post" id="regform2">
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">City</label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="city" name="city" class="form-control" placeholder="City" required="required" >
                                                <input type="hidden" name="_method" value="PUT">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Country</label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="country" name="country" class="form-control" placeholder="Country" required="required" >
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Cell No</label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="number" id="cell" name="cell" class="form-control" placeholder="Cell No">
                                            </div>
                                        </div>
                                    </form>
                                </div>


                                <div id="step-3" class="step">
                                    <form class="form-horizontal form-label-left">
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Select Role</label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <select class="select2_single form-control" tabindex="-1" name="role" id="role_selector">
                                                    <option value="customer">Customer</option>
                                                    <option value="counselor">Counselor</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div id="all" class="colors">
                                            <form class="form-horizontal form-label-left">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Certification</label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <input type="text" id="certification" name="certification" class="form-control" placeholder="Certification" required="required" >
                                                        <input type="hidden" name="_method" value="PUT">
                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Qualification</label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <textarea rows="5" cols="30" id="qualification" name="qualification" class="form-control" placeholder="qualification" required="required" >
                                                        </textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Designation</label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <input type="text" id="designation" name="designation" class="form-control" placeholder="Designation" required="required" >
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Skills</label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <input id="tags_1" name="skills" type="text" class="tags form-control" value="social, adverts, sales" />
                                                        <div id="suggestions-container" style="position: relative; float: left; width: 250px; margin: 10px;"></div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        {{--s--}}
                                    </form>
                                </div>


                                <div id="step-4" class="step">
                                    <form class="form-horizontal form-label-left" action="{{url('SignUp/save')}}" method="post" id="regform4">
                                        <div class="form-group">
                                            <label class="control-label col-md-4 col-sm-3 col-xs-12">This section only for Counselors</label>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Working Experience</label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" id="experience" name="experience" class="form-control" placeholder="Experience" required="required" >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Industry</label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <select class="select2_single form-control" tabindex="-1" id="industry">
                                                    <option>Industry</option>
                                                    <option value="information-technology">Information Technology</option>
                                                    <option value="education-training">Education/Training</option>
                                                    <option value="engineering">Engineering</option>
                                                    <option value="services">Services</option>
                                                    <option value="telecommunication-isp">Telecommunication / ISP</option>
                                                    <option value="advertising-pr">Advertising / PR</option>
                                                    <option value="banking-financial-services">Banking/Financial Services</option>
                                                    <option value="consultants">Consultants</option>
                                                    <option value="business-development">Business Development</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Functional Area</label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <select class="select2_single form-control" tabindex="-1" id="functionalarea">
                                                    <option>Functional Area</option>
                                                    <option value="Administration">Administration </option>
                                                    <option value="Advertising">Advertising </option>
                                                    <option value="Architects / Construction">Architects &amp; Construction </option>
                                                    <option value="Client Services / Customer Support">Client Services &amp; Customer Support </option>
                                                    <option value="Computer Networking">Computer Networking </option>
                                                    <option value="Corporate Affairs">Corporate Affairs </option>
                                                    <option value="Creative Design">Creative Design </option>
                                                    <option value="Data Entry">Data Entry </option>
                                                    <option value="Database Administration (DBA)">Database Administration (DBA) </option>
                                                    <option value="Distribution / Logistics">Distribution &amp; Logistics </option>
                                                    <option value="Engineering">Engineering </option>
                                                    <option value="Executive Management">Executive Management </option>
                                                    <option value="Hardware">Hardware </option>
                                                    <option value="Health / Medicine">Health &amp; Medicine </option>
                                                    <option value="Hotel/Restaurant Management">Hotel/Restaurant Management </option>
                                                    <option value="Human Resources">Human Resources </option>
                                                    <option value="Import / Export">Import &amp; Export </option>
                                                    <option value="Industrial Production">Industrial Production </option>
                                                    <option value="Legal Affairs">Legal Affairs </option>
                                                    <option value="Management Consulting">Management Consulting </option>
                                                    <option value="Management Information System (MIS)">Management Information System (MIS) </option>
                                                    <option value="Manufacturing">Manufacturing </option>
                                                    <option value="Marketing">Marketing </option>
                                                    <option value="Media - Print / Electronic">Media - Print &amp; Electronic </option>
                                                    <option value="Planning / Development">Planning &amp; Development </option>
                                                    <option value="Product Development">Product Development </option>
                                                    <option value="Product Management">Product Management </option>
                                                    <option value="Project Management">Project Management </option>
                                                    <option value="Public Relations">Public Relations </option>
                                                    <option value="Quality Assurance (QA)">Quality Assurance (QA) </option>
                                                    <option value="Quality Control">Quality Control </option>
                                                    <option value="Researcher">Researcher </option>
                                                    <option value="Sales / Business Development">Sales &amp; Business Development </option>
                                                    <option value="Search Engine Optimization (SEO)">Search Engine Optimization (SEO) </option>
                                                    <option value="Security">Security </option>
                                                    <option value="Software / Web Development">Software &amp; Web Development </option>
                                                    <option value="Supply Chain Management">Supply Chain Management </option>
                                                    <option value="Systems Analyst">Systems Analyst </option>
                                                    <option value="Teachers/Education, Training / Development">Teachers/Education, Training &amp; Development </option>
                                                    <option value="Writer">Writer </option>
                                                </select>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                            </div>
                            <!-- End SmartWizard Content -->
                            <div class="separate-link">
                                <p>If already exist
                                    <a href="{{url('Account-Login')}}" class="to_register"> Login </a>
                                </p>
                            </div>
                            <div id="todo"></div>
                        </div>

                    </div>

                </div>
            </div>

        </div>
    </div>
</div>

<!-- jQuery --><script src="{{ asset("vendors/backend_vendors/jquery/dist/jquery.min.js")}}"></script>

{{--<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>--}}
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

<!-- Bootstrap -->
<script src="{{ asset("vendors/backend_vendors/bootstrap/dist/js/bootstrap.min.js")}}"></script>
<!-- FastClick -->
<script src="{{ asset("vendors/backend_vendors/fastclick/lib/fastclick.js")}}"></script>
<!-- NProgress -->
<script src="{{ asset("vendors/backend_vendors/nprogress/nprogress.js")}}"></script>
<!-- iCheck -->
<script src="{{ asset("vendors/backend_vendors/iCheck/icheck.min.js")}}"></script>
<!-- bootstrap-datetimepicker -->
<script src="{{ asset("vendors/backend_vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js")}}"></script>
<!-- jQuery Tags Input -->
<script src="{{ asset("vendors/backend_vendors/jquery.tagsinput/src/jquery.tagsinput.js")}}"></script>
<!-- jQuery Smart Wizard -->
<script src="{{ asset("vendors/backend_vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js")}}"></script>
<!-- Custom Theme Scripts -->
<script src="{{ asset("build/backend_build/js/custom.min.js")}}"></script>

<script type="text/javascript">
    $(function() {

//        $('#counselor').hide();
//
//        $('#role_selector').change(function(){
//            $('.colors').hide();
//            $('#' + $(this).val()).show();
//        });

    });


    $(document).ready(function(){

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $(".buttonFinish").click(function () {

                var username = $("#username").val();
                var email =$("#email").val();
                var password =$("#password").val();
                var nickname =$("#nickname").val();
                var gender = $("input[name='gender']:checked").val();
                var date = $("#dob").val();
//                var date = new Date($('#single_cal4').val());
//                    var day = date.getDate();
//                    var month = date.getMonth() + 1;
//                    var year = date.getFullYear();
//                    var dob = day+"/"+month+"/"+year;
                var city =$("#city").val();
                var country =$("#country").val();
                var cell =$("#cell").val();
                var skills = $("#tags_1").val();
                var experience =$("#experience").val();
                var certification =$("#certification").val();
                var designation =$("#designation").val();
                var industry =$("#industry").val();
                var functionalarea =$("#functionalarea").val();
                var qualification =$("#qualification").val();
                var role =$("#role_selector").val();

                var data1 = $("#regform1").serialize();
                var data2 = $("#regform2").serialize();
                var data3 = $("#regform3").serialize();
                var data4 = $("#regform4").serialize();

                var delay = 4000;

                    $.ajax({
                        url:"{{url('Account-SignUp/save')}}",
                        type:'get',
                        data:{username:username,password:password,nickname:nickname,email:email,date:date,gender:gender,city:city,country:country,cell:cell,role:role,experience:experience,
                            skills:skills,industry:industry,functionalarea:functionalarea,certification:certification,designation:designation,qualification:qualification},
                        success:function(){
                            $('#todo').html('');
                            $('#todo').show();
                            $('#todo').append('Success').css("color","green");

                            setTimeout(function() {
                                delaySuccess(data);
                            }, delay);
                            window.location = "{{url('Account-Login')}}";
                        },
                        error: function (request) {
                            json = $.parseJSON(request.responseText);
                            $('#todo').html('');
                            $('#todo').show();
                            $.each(json.errors, function(key, value){
                                $('#todo').append('<p>'+value+'</p>').css("color","red");
                            });
                            $("#result").html('');
                        }
                    });

//                alert(data1+"-"+data2+"-"+data3+"-"+data4);
//                alert("Username ="+username+" , Email ="+email+" , Password ="+password+" , Nickname ="+nickname+" , Gender = "+gender+" , DOB ="+dob);
//                alert("City ="+city+" , Country ="+country+" , Cell ="+cell+" , Exp ="+exp+" , Certification = "+certification+" , Designation ="+design);
//                alert("Skills ="+skills+" , Industry ="+industry+" , Functional ="+functional+" , Role ="+role);
            });



    });


</script>
<script>
    !function(a){var b="object"==typeof self&&self.self===self&&self||"object"==typeof global&&global.global===global&&global;"function"==typeof define&&define.amd?define(["exports"],function(c){
        b.ParticleNetwork=a(b,c)}):"object"==typeof module&&module.exports?module.exports=a(b,{}):b.ParticleNetwork=a(b,{})}(function(a,b){
        var c=function(a){this.canvas=a.canvas,this.g=a.g,this.particleColor=a.options.particleColor,this.x=Math.random()*this.canvas.width,this.y=Math.random()*this.canvas.height,this.velocity={x:(Math.random()-.5)*a.options.velocity,y:(Math.random()-.5)*a.options.velocity}};
        return c.prototype.update=function(){(this.x>this.canvas.width+20||this.x<-20)&&(this.velocity.x=-this.velocity.x),
        (this.y>this.canvas.height+20||this.y<-20)&&(this.velocity.y=-this.velocity.y),
            this.x+=this.velocity.x,this.y+=this.velocity.y},c.prototype.h=function(){
            this.g.beginPath(),this.g.fillStyle=this.particleColor,this.g.globalAlpha=.7,this.g.arc(this.x,this.y,1.5,0,2*Math.PI),
                this.g.fill()},b=function(a,b){this.i=a,this.i.size={width:this.i.offsetWidth,height:this.i.offsetHeight},
            b=void 0!==b?b:{},this.options={particleColor:void 0!==b.particleColor?b.particleColor:"#fff",
            background:void 0!==b.background?b.background:"#1a252f",interactive:void 0!==b.interactive?b.interactive:!0,
            velocity:this.setVelocity(b.speed),density:this.j(b.density)},this.init()},
            b.prototype.init=function(){if(this.k=document.createElement("div"),this.i.appendChild(this.k),
                    this.l(this.k,{position:"absolute",top:0,left:0,bottom:0,right:0,"z-index":1}),/(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i.test(this.options.background))this.l(this.k,
                {background:this.options.background});else{if(!/\.(gif|jpg|jpeg|tiff|png)$/i.test(this.options.background))return console.error("Please specify a valid background image or hexadecimal color"),
                !1;this.l(this.k,{background:'url("'+this.options.background+'") no-repeat center',"background-size":"cover"})}if(!/(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i.test(this.options.particleColor))
                return console.error("Please specify a valid particleColor hexadecimal color"),!1;
                this.canvas=document.createElement("canvas"),this.i.appendChild(this.canvas),
                    this.g=this.canvas.getContext("2d"),this.canvas.width=this.i.size.width,this.canvas.height=this.i.size.height,this.l(this.i,
                    {position:"inherit"}),this.l(this.canvas,{"z-index":"20",position:"relative"}),window.addEventListener("resize",function(){
                    return this.i.offsetWidth===this.i.size.width&&this.i.offsetHeight===this.i.size.height?!1:(this.canvas.width=this.i.size.width=this.i.offsetWidth,
                        this.canvas.height=this.i.size.height=this.i.offsetHeight,clearTimeout(this.m),void(this.m=setTimeout(function(){
                        this.o=[];for(var a=0;a<this.canvas.width*this.canvas.height/this.options.density;a++)this.o.push(new c(this));
                        this.options.interactive&&this.o.push(this.p),requestAnimationFrame(this.update.bind(this))}.bind(this),500)))}.bind(this)),
                    this.o=[];for(var a=0;a<this.canvas.width*this.canvas.height/this.options.density;a++)this.o.push(new c(this));
                this.options.interactive&&(this.p=new c(this),this.p.velocity={x:0,y:0},this.o.push(this.p),
                    this.canvas.addEventListener("mousemove",function(a){this.p.x=a.clientX-this.canvas.offsetLeft,
                        this.p.y=a.clientY-this.canvas.offsetTop}.bind(this)),this.canvas.addEventListener("mouseup",function(a){this.p.velocity={
                    x:(Math.random()-.5)*this.options.velocity,y:(Math.random()-.5)*this.options.velocity},
                    this.p=new c(this),this.p.velocity={x:0,y:0},this.o.push(this.p)}.bind(this))),requestAnimationFrame(this.update.bind(this))},
            b.prototype.update=function(){this.g.clearRect(0,0,this.canvas.width,this.canvas.height),this.g.globalAlpha=1;for(var a=0;a<this.o.length;a++){
                this.o[a].update(),this.o[a].h();for(var b=this.o.length-1;b>a;b--){var c=Math.sqrt(Math.pow(this.o[a].x-this.o[b].x,2)+Math.pow(this.o[a].y-this.o[b].y,2));c>120||(this.g.beginPath(),
                    this.g.strokeStyle=this.options.particleColor,this.g.globalAlpha=(120-c)/120,this.g.lineWidth=.7,this.g.moveTo(this.o[a].x,
                    this.o[a].y),this.g.lineTo(this.o[b].x,this.o[b].y),this.g.stroke())}}0!==this.options.velocity&&requestAnimationFrame(this.update.bind(this))},
            b.prototype.setVelocity=function(a){return"fast"===a?1:"slow"===a?.33:"none"===a?0:.66},
            b.prototype.j=function(a){return"high"===a?5e3:"low"===a?2e4:isNaN(parseInt(a,10))?1e4:a},
            b.prototype.l=function(a,b){for(var c in b)a.style[c]=b[c]},b});

    // Initialisation

    var canvasDiv = document.getElementById('particle-canvas');
    var options = {
        particleColor: '#2A3F54',
        background: '{{asset('images/backend_images/img/header-bg-2.png')}}',
        interactive: true,
        speed: 'medium',
        density: 'high',

    };
    var particleCanvas = new ParticleNetwork(canvasDiv, options);
</script>

</body>
</html>
