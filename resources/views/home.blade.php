@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                        {{--@if($userStatus)--}}
                            {{--@foreach($userStatus as $online)--}}

                                {{--@if($online->isOnline())--}}
                                    {{--<span>{{$online->name."Online"}}</span>--}}
                                {{--@endif--}}
                            {{--@endforeach--}}
                        {{--@endif--}}
                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
