<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSocialLinksInCounselorsColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('counselors', function (Blueprint $table) {
            //
            $table->string('facebook_link')->nullable()->after('functional_area');
            $table->string('linkedin_link')->nullable()->after('facebook_link');
            $table->string('twitter_link')->nullable()->after('linkedin_link');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('counselors', function (Blueprint $table) {
            //
        });
    }
}
